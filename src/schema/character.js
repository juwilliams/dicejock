import React from 'react';
import { Button } from 'ui-kit';

export const Alignment = {
    LAWFUL_GOOD: 'Lawful Good',
    NEUTRAL_GOOD: 'Neutral Good',
    CHAOTIC_GOOD: 'Chaotic Good',
    LAWFUL_NEUTRAL: 'Lawful Neutral',
    NEUTRAL: 'Neutral',
    CHAOTIC_NEUTRAL: 'Chaotic Neutral',
    LAWFUL_EVIL: 'Lawful Evil',
    NEUTRAL_EVIL: 'Neutral Evil',
    CHAOTIC_EVIL: 'Chaotic Evil'
};
export const Alignments = [
    Alignment.LAWFUL_GOOD,
    Alignment.NEUTRAL_GOOD,
    Alignment.CHAOTIC_GOOD,
    Alignment.LAWFUL_NEUTRAL,
    Alignment.NEUTRAL,
    Alignment.CHAOTIC_NEUTRAL,
    Alignment.LAWFUL_EVIL,
    Alignment.NEUTRAL_EVIL,
    Alignment.CHAOTIC_EVIL
];
export const AlignmentOptions = Alignments.map((a) => ({ key: a, label: a, value: a }));

export const characterGridHeaders = [
    {
        key: 'character-grid-header-0',
        label: 'Name',
        field: 'name',
        style: {
            width: 200
        },
        renderRowCell: (item, onIdClick) => (
            <Button onClick={() => (onIdClick ? onIdClick(item) : undefined)} value={item.name} small rowaction />
        )
    },
    {
        key: 'character-grid-header-1',
        label: 'Template',
        field: 'templateName',
        style: {
            width: 100
        }
    },
    {
        key: 'character-grid-header-2',
        label: 'Alignment',
        field: 'alignment',
        style: {
            width: 100
        }
    },
    {
        key: 'character-grid-header-3',
        label: 'Hit Points',
        field: 'hitPoints',
        style: {
            width: 100
        },
        renderRowCell: (item) => <span>{item?.attributes?.find((attr) => attr.name === 'Hit Points')?.value}</span>
    },
    {
        key: 'character-grid-header-4',
        label: 'Level',
        field: 'level',
        style: {
            width: 180
        }
    },
    {
        key: 'character-grid-header-5',
        label: 'Faction',
        field: 'faction',
        style: {
            width: 180
        }
    }
];
