import React from 'react';

import { Button } from 'ui-kit';

export const campaignGridHeaders = [
  {
    key: 'campaign-grid-header-0',
    label: 'Name',
    field: 'name',
    style: {
      width: 200
    },
    renderRowCell: (item, onIdClick) => (
      <Button onClick={() => (onIdClick ? onIdClick(item) : undefined)} value={item.name} small rowaction />
    )
  },
  {
    key: 'campaign-grid-header-1',
    label: 'Chapter',
    field: 'chapter',
    style: {
      width: 100
    }
  },
  {
    key: 'campaign-grid-header-2',
    label: 'Players',
    field: 'players',
    style: {
      width: 100
    },
    renderRowCell: (item) => <div className="grid-cell">{item.players ? item.players.length : 0}</div>
  },
  {
    key: 'campaign-grid-header-3',
    label: 'Last Session',
    field: 'lastSession',
    style: {
      width: 180
    }
  }
];

export default {
  campaignGridHeaders
};
