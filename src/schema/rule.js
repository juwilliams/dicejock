import React from 'react';

import { Button } from 'ui-kit';

export const ruleGridHeaders = [
  {
    key: 'campaign-grid-header-0',
    label: 'Name',
    field: 'name',
    style: {
      width: 200
    },
    renderRowCell: (item, onIdClick) => (
      <Button onClick={() => (onIdClick ? onIdClick(item) : undefined)} value={item.name} small rowaction />
    )
  },
  {
    key: 'campaign-grid-header-2',
    label: 'Type',
    field: 'type',
    style: {
      width: 80
    }
  },
  {
    key: 'campaign-grid-header-3',
    label: 'Trigger',
    field: 'trigger',
    style: {
      width: 80
    }
  },
  {
    key: 'campaign-grid-header-4',
    label: 'Affect',
    field: 'affect',
    style: {
      width: 140
    }
  },
  {
    key: 'campaign-grid-header-5',
    label: 'Target',
    field: 'target',
    style: {
      width: 80
    }
  },
  {
    key: 'campaign-grid-header-6',
    label: 'Value',
    field: 'value',
    style: {
      width: 80
    }
  }
];

export const ruleTypeOptions = [
  {
    key: 'rule-type-options-1',
    value: 'ADDITIVE',
    label: 'Additive'
  },
  {
    key: 'rule-type-options-2',
    value: 'DESCRIPTIVE',
    label: 'Descriptive'
  },
  {
    key: 'rule-type-options-3',
    value: 'DURATION',
    label: 'Duration'
  },
  {
    key: 'rule-type-options-4',
    value: 'EXPONENTIAL',
    label: 'Exponential'
  },
  {
    key: 'rule-type-options-5',
    value: 'SUBTRACTIVE',
    label: 'Subtractive'
  },
  {
    key: 'rule-type-options-6',
    value: 'STATIC',
    label: 'Static'
  }
];

export const ruleAffectOptions = [
  {
    key: 'rule-affect-options-1',
    value: 'ATTRIBUTE',
    label: 'Attribute'
  },
  {
    key: 'rule-affect-options-2',
    value: 'FEAT',
    label: 'Feat'
  },
  {
    key: 'rule-affect-options-3',
    value: 'ITEM',
    label: 'Item'
  },
  {
    key: 'rule-affect-options-4',
    value: 'RULE',
    label: 'Rule'
  },
  {
    key: 'rule-affect-options-5',
    value: 'SKILL',
    label: 'Skill'
  }
];

export const ruleTriggerOptions = [
  {
    key: 'rule-trigger-options-1',
    value: 'COLLISION',
    label: 'Collision'
  },
  {
    key: 'rule-trigger-options-2',
    value: 'MOVEMENT',
    label: 'Movement'
  },
  {
    key: 'rule-trigger-options-3',
    value: 'TARGETED',
    label: 'Targeted'
  },
  {
    key: 'rule-trigger-options-4',
    value: 'TOUCH',
    label: 'Touch'
  },
  {
    key: 'rule-trigger-options-5',
    value: 'TRIGGERED',
    label: 'Triggered'
  }
];

export default {
  ruleGridHeaders,
  ruleTypeOptions
};
