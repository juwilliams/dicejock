import React from 'react';

import { Button } from 'ui-kit';

export const templateGridHeaders = [
  {
    key: 'campaign-grid-header-0',
    label: 'Name',
    field: 'name',
    style: {
      width: 200
    },
    renderRowCell: (item, onIdClick) => (
      <Button onClick={() => (onIdClick ? onIdClick(item) : undefined)} value={item.name} small nopadding rowaction />
    )
  },
  {
    key: 'campaign-grid-header-2',
    label: 'Rules',
    field: 'rules',
    style: {
      width: 100
    },
    renderRowCell: (item) => <>{item.rules ? item.rules.length : 0}</>
  },
  {
    key: 'campaign-grid-header-3',
    label: 'Type',
    field: 'type',
    style: {
      width: 180
    }
  },
  {
    key: 'campaign-grid-header-1',
    label: 'Description',
    field: 'description',
    style: {
      width: '100%'
    }
  }
];

export const templateTypes = {
  CHARACTER: 'CHARACTER',
  CLASS: 'CLASS',
  GAME: 'GAME',
  ITEM: 'ITEM'
};

export const templateTypeOptions = [
  {
    key: 'template-type-option-0',
    value: templateTypes.CHARACTER,
    label: 'Character Template',
    disabledPageIds: []
  },
  {
    key: 'template-type-option-1',
    value: templateTypes.CLASS,
    label: 'Class Template',
    disabledPageIds: []
  },
  {
    key: 'template-type-option-2',
    value: templateTypes.GAME,
    label: 'Game Template',
    disabledPageIds: []
  },
  {
    key: 'template-type-option-3',
    value: templateTypes.ITEM,
    label: 'Item Template',
    disabledPageIds: []
  }
];

export default {
  templateGridHeaders,
  templateTypes,
  templateTypeOptions
};
