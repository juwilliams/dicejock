import React from 'react';

import { Button } from 'ui-kit';

export const attributeGridHeaders = [
  {
    key: 'template-attribute-grid-header-0',
    label: 'Name',
    field: 'name',
    style: {
      width: 200
    },
    renderRowCell: (item, onIdClick) => (
      <Button onClick={() => (onIdClick ? onIdClick(item) : undefined)} value={item.name} small rowaction />
    )
  },
  {
    key: 'template-attribute-grid-header-2',
    label: 'Type',
    field: 'type',
    style: {
      width: 120
    }
  },
  {
    key: 'template-attribute-grid-header-3',
    label: 'Min Value',
    field: 'minValue',
    style: {
      width: 80
    }
  },
  {
    key: 'template-attribute-grid-header-4',
    label: 'Max Value',
    field: 'maxValue',
    style: {
      width: 80
    }
  },
  {
    key: 'template-attribute-grid-header-5',
    label: 'Description',
    field: 'description'
  }
];

export const attributeTypeOptions = [
  {
    key: 'template-attribute-type-option-0',
    value: 'RANGE',
    label: 'Range'
  },
  {
    key: 'template-attribute-type-option-1',
    value: 'DAMAGE',
    label: 'Damage'
  },
  {
    key: 'template-attribute-type-option-2',
    value: 'DAMAGE_MAGICAL',
    label: 'Damage (Magic)'
  },
  {
    key: 'template-attribute-type-option-3',
    value: 'STATIC',
    label: 'Static'
  }
];

export const attributeAvailableToOptions = [
  {
    key: 'template-attribute-available-to-option-0',
    value: 'CHARACTER',
    label: 'Character'
  },
  {
    key: 'template-attribute-available-to-option-1',
    value: 'EVENT',
    label: 'Event'
  },
  {
    key: 'template-attribute-available-to-option-2',
    value: 'ITEM',
    label: 'Item'
  },
  {
    key: 'template-attribute-available-to-option-3',
    value: 'PLAYER',
    label: 'Player'
  },
  {
    key: 'template-attribute-available-to-option-4',
    value: 'SELF',
    label: 'Self'
  }
];

export default {
  attributeGridHeaders,
  attributeTypeOptions,
  attributeAvailableToOptions
};
