import React from 'react';

import { Button } from 'ui-kit';

export const featGridHeaders = [
  {
    key: 'template-feat-grid-header-0',
    label: 'Name',
    field: 'name',
    style: {
      width: 200
    },
    renderRowCell: (item, onIdClick) => (
      <Button onClick={() => (onIdClick ? onIdClick(item) : undefined)} value={item.name} small rowaction />
    )
  },
  {
    key: 'template-feat-grid-header-1',
    label: 'Affects Type',
    field: 'affectsType',
    style: {
      width: 120,
      minWidth: 120
    }
  },
  {
    key: 'template-feat-grid-header-2',
    label: 'Affects',
    field: 'affects',
    style: {
      width: 120,
      minWidth: 120
    }
  },
  {
    key: 'template-feat-grid-header-3',
    label: 'Value',
    field: 'value',
    style: {
      width: 80,
      minWidth: 80
    }
  },
  {
    key: 'template-feat-grid-header-4',
    label: 'Duration Type',
    field: 'durationType',
    style: {
      width: 120,
      minWidth: 120
    }
  },
  {
    key: 'template-feat-grid-header-5',
    label: 'Duration',
    field: 'duration',
    style: {
      width: 80,
      minWidth: 80
    }
  },
  {
    key: 'template-feat-grid-header-7',
    label: 'Require Attribute',
    field: 'requiredAttribute',
    style: {
      width: 120,
      minWidth: 120
    }
  },
  {
    key: 'template-feat-grid-header-8',
    label: 'Req. Attr. Value',
    field: 'description',
    style: {
      width: 120,
      minWidth: 120
    }
  },
  {
    key: 'template-feat-grid-header-9',
    label: 'Description',
    field: 'description'
  }
];

export const featAffectsTypes = {
  ATTRIBUTE: 'ATTRIBUTE',
  SKILL: 'SKILL',
  SKILL_RANK: 'SKILL_RANK',
  DC_CHECK: 'DC_CHECK'
};

export const featAffectsTypeOptions = [
  {
    key: 'template-feat-affects-option-0',
    value: featAffectsTypes.ATTRIBUTE,
    label: 'Attribute'
  },
  {
    key: 'template-feat-affects-option-1',
    value: featAffectsTypes.SKILL,
    label: 'Skill'
  },
  {
    key: 'template-feat-affects-option-2',
    value: featAffectsTypes.SKILL_RANK,
    label: 'Skill Rank'
  },
  {
    key: 'template-feat-affects-option-3',
    value: featAffectsTypes.DC_CHECK,
    label: 'DC Check'
  }
];

export const featDurationTypeOptions = [
  {
    key: 'template-feat-duration-option-1',
    value: 'PERMANENT',
    label: 'Permanent'
  },
  {
    key: 'template-feat-duration-option-0',
    value: 'TEMPORARY',
    label: 'Temporary'
  }
];

export default {
  featGridHeaders,
  featAffectsTypes,
  featAffectsTypeOptions,
  featDurationTypeOptions
};
