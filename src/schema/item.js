import React from 'react';
import { Button } from 'ui-kit';

export const itemGridHeaders = [
    {
        key: 'item-grid-header-0',
        label: 'Name',
        field: 'name',
        style: {
            width: 200
        },
        renderRowCell: (item, onIdClick) => (
            <Button onClick={() => (onIdClick ? onIdClick(item) : undefined)} value={item.name} small rowaction />
        )
    },
    {
        key: 'item-grid-header-1',
        label: 'Template',
        field: 'templateName',
        style: {
            width: 100
        }
    }
];
