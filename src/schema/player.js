import React from 'react';

import { Button } from 'ui-kit';

export const playerGridHeaders = [
  {
    key: 'player-grid-header-0',
    label: 'Name',
    field: 'name',
    style: {
      width: 200
    },
    renderRowCell: (item, onIdClick) => (
      <Button onClick={() => (onIdClick ? onIdClick(item) : undefined)} value={item.name} small rowaction />
    )
  },
  {
    key: 'player-grid-header-1',
    label: 'Level',
    field: 'level',
    style: {
      width: 100
    }
  },
  {
    key: 'player-grid-header-2',
    label: 'Class',
    field: 'class',
    style: {
      width: 100
    }
  },
  {
    key: 'player-grid-header-3',
    label: 'Experience to Level',
    field: 'experienceToLevel',
    style: {
      width: 180
    }
  },
  {
    key: 'player-grid-header-4',
    label: 'Campaign',
    field: 'campaign',
    style: {
      width: 180
    }
  },
  {
    key: 'player-grid-header-5',
    label: 'Note',
    field: 'note',
    style: {
      width: 180
    }
  }
];

export default {
  playerGridHeaders
};
