import React from 'react';

import { Button } from 'ui-kit';

export const EventTrigger = {
    BOUNDARY: 'Boundary',
    DAMAGE: 'Damage',
    DEATH: 'Death',
    OBSERVATION: 'Observation',
    ELEMENTAL: 'Elemental',
    MOVEMENT: 'Movement',
    SOUND: 'Sound',
    TOUCH: 'Touch',
    SPIRITUAL: 'Spirtual'
};

export const EventTriggers = [
    EventTrigger.BOUNDARY,
    EventTrigger.DAMAGE,
    EventTrigger.DEATH,
    EventTrigger.OBSERVATION,
    EventTrigger.ELEMENTAL,
    EventTrigger.MOVEMENT,
    EventTrigger.SOUND,
    EventTrigger.TOUCH,
    EventTrigger.SPIRITUAL
];

export const EventTriggerOptions = EventTriggers.map((e) => ({ key: e, label: e, value: e }));

export const eventGridHeaders = [
    {
        key: 'character-grid-header-0',
        label: 'Name',
        field: 'name',
        style: {
            width: 200
        },
        renderRowCell: (item, onIdClick) => (
            <Button onClick={() => (onIdClick ? onIdClick(item) : undefined)} value={item.name} small rowaction />
        )
    },
    {
        key: 'character-grid-header-1',
        label: 'Location',
        field: 'location',
        style: {
            width: 100
        }
    },
    {
        key: 'character-grid-header-2',
        label: 'Trigger',
        field: 'trigger',
        style: {
            width: 100
        }
    }
];
