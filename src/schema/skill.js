import React from 'react';

import { Button } from 'ui-kit';

export const skillGridHeaders = [
  {
    key: 'template-skill-grid-header-0',
    label: 'Name',
    field: 'name',
    style: {
      width: 200
    },
    renderRowCell: (item, onIdClick) => (
      <Button onClick={() => (onIdClick ? onIdClick(item) : undefined)} value={item.name} small rowaction />
    )
  },
  {
    key: 'template-skill-grid-header-2',
    label: 'Difficulty',
    field: 'difficulty',
    style: {
      width: 80
    }
  },
  {
    key: 'template-skill-grid-header-3',
    label: 'Dependent Attribute',
    field: 'dependentAttribute',
    style: {
      width: 180,
      minWidth: 180
    }
  },
  {
    key: 'template-skill-grid-header-4',
    label: 'Min. Dep. Attr. Value',
    field: 'minDependentAttributeValue',
    style: {
      width: 180,
      minWidth: 180
    }
  },
  {
    key: 'template-skill-grid-header-5',
    label: 'Description',
    field: 'description',
    style: {
      width: 240,
      minWidth: 240
    }
  }
];

export default {
  skillGridHeaders
};
