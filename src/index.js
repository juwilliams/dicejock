import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import { App } from 'components';
import { About, Campaign, Designer, Features, Join, Journal, Login, Logout, Player, Register, Template } from 'views';

import { ProtectedRoute } from 'hoc';

import * as serviceWorker from 'serviceWorker';

import store from 'store';

import './index.css';
import { DialogSystem, NotificationSystem } from 'ui-kit';

ReactDOM.render(
    <div id="app-root">
        <Provider store={store}>
            <BrowserRouter>
                <App>
                    <NotificationSystem />
                    <DialogSystem />
                    <Switch>
                        <Route path="/about" component={About} />
                        <ProtectedRoute path="/designer" component={Designer} />
                        <ProtectedRoute exact path="/campaign" component={Campaign} />
                        <ProtectedRoute path="/campaign/:id" component={Campaign} />
                        <ProtectedRoute exact path="/campaign/join/:invite" component={Join} />
                        <ProtectedRoute exact path="/journal" component={Journal} />
                        <ProtectedRoute path="/player" component={Player} />
                        <ProtectedRoute exact path="/template" component={Template} />
                        <ProtectedRoute path="/template/:type/:id" component={Template} />
                        <Route path="/features" component={Features} />
                        <Route path="/login" component={Login} />
                        <Route path="/logout" component={Logout} />
                        <Route path="/register" component={Register} />
                        <Redirect from="/" to="/about" />
                    </Switch>
                </App>
            </BrowserRouter>
        </Provider>
    </div>,
    document.getElementById('root')
);

serviceWorker.unregister();
