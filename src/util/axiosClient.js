import axios from 'axios';
import { userLoggedOut } from 'state/auth/auth.reducer';

const axiosClient = axios.create();
axiosClient.defaults.withCredentials = true;
axiosClient.defaults.headers = { 'Content-Type': 'application/json' };

const setLoggedOutInterceptor = (store) => {
    axiosClient.interceptors.response.use(
        (response) => response,
        (error) => {
            //	force the user to log out of the system if an API response indicates that their token is no longer authorized
            if (error.response && (error.response.status === 401 || error.response.status === 403)) {
                store.dispatch(userLoggedOut());
            }
        }
    );
};

//	header helpers
const setDefaultHeader = (name, value) => {
    axiosClient.defaults.headers[name] = value;
};
const unsetDefaultHeader = (name) => {
    delete axiosClient.defaults.headers[name];
};
const getDefaultHeader = (name) => axiosClient.defaults.headers[name];

export default axiosClient;

export { getDefaultHeader, setLoggedOutInterceptor, setDefaultHeader, unsetDefaultHeader };
