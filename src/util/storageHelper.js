import { setDefaultHeader } from './axiosClient';

const authTokenLocalStorageKey = 'Authorization';

export default {
	local: {
		getAuthToken: () => localStorage.getItem(authTokenLocalStorageKey),
		setAuthToken: token => {
			localStorage.setItem(authTokenLocalStorageKey, token);

			setDefaultHeader('Authorization', `Bearer ${token}`);
		},
		removeAuthToken: () => localStorage.removeItem(authTokenLocalStorageKey)
	}
};
