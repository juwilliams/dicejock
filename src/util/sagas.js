import { call } from 'redux-saga/effects';

export function* baseEffectHandler({ service, data, onResponse, onError }) {
	try {
		const response = yield call(service, data);
		if (!response.error) {
			if (onResponse) yield onResponse(response);
		} else {
			throw response.error;
		}
	} catch (err) {
		console.log(err);
		if (onError) yield onError(err);
	}
}

export default { baseEffectHandler };
