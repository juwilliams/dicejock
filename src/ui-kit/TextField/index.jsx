import React from 'react';

import classNames from 'classnames';

import './style.css';

const TextField = props => {
	const {
		disabled,
		fullwidth,
		onChange,
		label,
		labelControls,
		multiline,
		password,
		placeholder,
		rows = 15,
		defaultValue
	} = props;

	const classes = classNames('text-field', { fullwidth });

	return (
		<div className="text-field">
			<div className="text-field-label-wrapper">
				<span className="text-field-label">{label}</span>
				<div className="text-field-label-controls">{labelControls}</div>
			</div>
			{!multiline && (
				<input
					disabled={disabled}
					type={password ? 'password' : 'text'}
					className={classes}
					defaultValue={defaultValue}
					onChange={e => (onChange ? onChange(e) : null)}
					placeholder={placeholder || ''}
				/>
			)}
			{multiline && (
				<textarea
					disabled={disabled}
					className={classNames({ fullwidth })}
					rows={`${rows}`}
					defaultValue={defaultValue}
					onChange={e => (onChange ? onChange(e) : null)}
					placeholder={placeholder || ''}
				/>
			)}
		</div>
	);
};

export default TextField;
