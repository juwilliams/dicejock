import React from 'react';
import Select from 'react-select';
import classNames from 'classnames';

import './style.css';

const ReactSelect = props => {
	const { className, label, options = [], onChange, defaultValue, isSearchable = false } = props;

	const classes = classNames('select', className);

	const selectStyles = {
		container: (provided) => ({
			...provided,
			minWidth: '240px',
			marginBottom: '1rem',
		}),
		control: (provided) => ({
		  ...provided,
		  border: "1px solid #aaaaaa",
		  borderRadius: "4px",
		  color: "#000000",
		  fontFamily: "Helvetica",
		  fontSize: "1.2rem",
		  padding: "0.3rem 0.5rem",
		  ":hover": {
			...provided[":hover"],
			border: "1px solid #000000",
			color: "#000000",
		  },
		}),
		input: (provided) => ({
			...provided,
			width: '100%',
		}),
		menu: (provided) => ({
		  ...provided,
		  border: "1px solid #000000",
		  borderRadius: "0",
		  fontFamily: "Helvetica",
		  fontSize: "1.2rem",
		}),
		option: (provided, { isSelected }) => ({
		  ...provided,
		  backgroundColor: isSelected ? "#000000" : "#ffffff",
		  color: isSelected ? "#ffffff" : "#000000",
		  padding: "0.8rem",
		  ":hover": {
			...provided[":hover"],
			backgroundColor: isSelected ? "#000000" : "rgb(234, 233, 233)",
			color: isSelected ? "#ffffff" : "#000000",
		  },
		}),
	  };

	const handleChange = option => {
		const { value } = option;
		if (onChange) onChange(options.find(o => o.value === value));
	};
	
	return (
		<div className={classes}>
			<div className="select-label">{label}</div>
			<Select
				key={defaultValue}
				isSearchable={isSearchable}
				onChange={handleChange}
				value={defaultValue}
				styles={selectStyles}
				options={options}
			/>
		</div>
	);
};

export default ReactSelect;
