import React from 'react';

import classNames from 'classnames';

import './style.css';

const PageInnard = props => {
	const { children, nopadding } = props;
	
	const classes = classNames('page-innard', { nopadding });

	return <div className={classes}>{children}</div>;
};

export default PageInnard;
