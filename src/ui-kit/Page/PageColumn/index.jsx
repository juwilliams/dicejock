import React from 'react';

import classNames from 'classnames';

import './style.css';

const PageColumn = props => {
	const { children, fullwidth, nopadding } = props;

	const classes = classNames('page-column', { fullwidth }, { nopadding });

	return <div className={classes}>{children}</div>;
};

export default PageColumn;
