import React from 'react';

import classNames from 'classnames';

import PageColumn from './PageColumn';
import PageInnard from './PageInnard';
import PageInnerNav from './PageInnerNav';
import PageRow from './PageRow';

import './style.css';

const Page = props => {
	const {
		children,
		interPageNavigation,
		pageArgs: { condenseHeader, darkMode, title, subTitle } = {},
		siteNavigation
	} = props;

	const classes = classNames(
		'page',
		{ 'dark-mode': darkMode },
		{ 'condense-header': condenseHeader }
	);

	return (
		<div className={classes}>
			{siteNavigation}
			{interPageNavigation && (
				<div className="inter-page-nav">{interPageNavigation}</div>
			)}
			<div className="page-content">
				{title && (
					<div className="page-title-wrapper">
						<h1 className="page-title">{title}</h1>
						{subTitle && <h3 className="page-sub-title">{subTitle}</h3>}
					</div>
				)}
				{children}
			</div>
		</div>
	);
};

export default Page;

export { PageColumn, PageInnard, PageInnerNav, PageRow };
