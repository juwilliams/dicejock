import React from 'react';

import './style.css';

const PageInnerNav = props => {
	const { children } = props;

	return <div className="page-inner-nav">{children}</div>;
};

export default PageInnerNav;
