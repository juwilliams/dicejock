import React from 'react';
import classNames from 'classnames';

import './style.css';

const PageRow = props => {
	const { children, fullwidth, nopadding } = props;

	const classes = classNames('page-row', { nopadding }, { fullwidth });

	return <div className={classes}>{children}</div>;
};

export default PageRow;
