import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import classNames from 'classnames';

import { dialogSelector } from 'state/dialog/dialog.selectors';

import { Button } from 'ui-kit';

import './style.css';

export const Dialog = props => {
	const { actions, children, onClose, open, title } = props;
	const [isOpen, setIsOpen] = useState(false);

	const handleClose = () => {
		if (onClose) onClose();
		setIsOpen(false);
	};

	useEffect(() => {
		setIsOpen(open);
	}, [open]);

	const classes = classNames('dialog-container', { open: isOpen });

	return (
		<div className={classes}>
			<div
				className="dialog-blanket"
				onClick={handleClose}
				role="button"
				tabIndex={0}
			/>
			<div className="dialog">
				<div className="dialog-header">
					<h1 className="dialog-title">{title}</h1>
				</div>
				<div className="dialog-content">{children}</div>
				<div className="dialog-actions">
					<Button onClick={handleClose} value="Cancel" />
					{actions}
				</div>
			</div>
		</div>
	);
};

const DialogSystem = () => {
	const { current: dialog } = useSelector(state => dialogSelector(state));

	if (!dialog) {
		return <div className="dialog-system" />
	}
	const { render: dialogRenderer, props: dialogProps } = dialog;
	return dialogRenderer(dialogProps);
}

export default DialogSystem;
