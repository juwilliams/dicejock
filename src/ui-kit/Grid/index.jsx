import React, { useEffect, useState } from 'react';
import classNames from 'classnames';

import './style.css';

const Grid = props => {
	const {
		actions,
		title = '',
		headers = [],
		fullwidth,
		data = [],
		multipleSelection,
		onIdClick,
		onSelection
	} = props;

	const [selectedCheckboxItem, setSelectedCheckboxItem] = useState(undefined);
	const [selectedCheckboxItems, setSelectedCheckboxItems] = useState([]);

	useEffect(() => {
		if (onSelection) {
			if (multipleSelection) {
				onSelection(selectedCheckboxItems);
			} else {
				onSelection(selectedCheckboxItem);
			}
		}
	}, [
		selectedCheckboxItem,
		selectedCheckboxItems,
		multipleSelection,
		onSelection
	]);

	const renderHeaders = () => (<div className="grid-headers grid-rows">
				<div className="grid-row">
					{onSelection && <div className="grid-cell grid-cell-selector" />}
					{headers.map(h => (
						<div
							key={`grid-header-${h.label}`}
							className="grid-cell"
							style={h.style}>
							{h.label}
						</div>
					))}
				</div>
			</div>);

	const renderRow = (item, classes) => {
		const rowClasses = classNames('grid-row', classes);

		const mappedCells = headers.map(h => (
			<div key={h.key} className="grid-cell" style={h.style}>
				{h.renderRowCell && h.renderRowCell(item, onIdClick)}
				{!h.renderRowCell && item[h.field]}
			</div>
		));
		

		return (
			<div key={`grid-row-${item.id}`} className={rowClasses}>
				{onSelection && (
					<div className="grid-cell grid-cell-selector">
						<input
							type="checkbox"
							key={`grid-row-checkbox-${item.id}`}
							onChange={() => {
								if (multipleSelection) {
									if (selectedCheckboxItems.find(i => i.id === item.id)) {
										setSelectedCheckboxItems([
											...selectedCheckboxItems.filter(i => i.id !== item.id)
										]);
									} else {
										setSelectedCheckboxItems([...selectedCheckboxItems, item]);
									}
								} else if (
									selectedCheckboxItem &&
									selectedCheckboxItem.id === item.id
								) {
									setSelectedCheckboxItem(undefined);
								} else {
									setSelectedCheckboxItem(item);
								}
							}}
							defaultChecked={
								multipleSelection
									? selectedCheckboxItems.find(i => i.id === item.id)
									: false
							}
						/>
					</div>
				)}
				{mappedCells}
			</div>
		);
	};

	const classes = classNames('grid', { fullwidth });

	return (
		<div className={classes}>
			<div className="grid-header">
				{title && <h2 className="grid-title">{title}</h2>}
				<div className="grid-actions">{actions}</div>
			</div>
			{renderHeaders()}
			<div className="grid-data grid-rows">{data.map(i => renderRow(i))}</div>
		</div>
	);
};

export default Grid;
