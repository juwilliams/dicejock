import React from 'react';
import { Link } from 'react-router-dom';

import classNames from 'classnames';

import './style.css';

const LinkButton = props => {
	const { children, isActive, to = '/', primary, small } = props;

	const classes = classNames(
		'link-button',
		{ active: isActive },
		{ primary },
		{ small }
	);

	return (
		<Link to={to} className={classes}>
			{children}
			{isActive && <hr />}
		</Link>
	);
};

export default LinkButton;
