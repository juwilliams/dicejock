import React from 'react';

import './style.css';

const Label = props => {
	const { value } = props;
	return <h5 className="label">{value}</h5>;
};

export default Label;
