import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import classNames from 'classnames';

// add icon

import './style.css';

const Button = props => {
	const {
		className,
		disabled = false,
		flat,
		hoverable,
		async,
		isBusy,
		nomargin,
		nomarginleft,
		onClick,
		primary,
		rowaction,
		secondary,
		small,
		value
	} = props;

	const classes = classNames(
		className,
		'button',
		{ flat },
		{ hoverable },
		{ primary },
		{ disabled: disabled || isBusy },
		{ nomargin },
		{ nomarginleft },
		{ rowaction },
		{ secondary },
		{ small }
	);

	const renderButton = () => (
		<>
			{disabled && <input disabled type="button" value={value} />}
			{!disabled && (
				<input
					type="button"
					value={value}
					onClick={e => (onClick ? onClick(e) : null)}
				/>
			)}
		</>
	);

	const renderAsyncButton = () => (
			<>
				{(disabled || isBusy) && (
					<div className="async-button">
						{isBusy && <FontAwesomeIcon icon="cog" className="fa-spin" />}
						{!isBusy && value}
					</div>
				)}
				{!disabled && !isBusy && (
					<div
						className="async-button"
						onClick={e => (onClick ? onClick(e) : null)}
						role="button"
						tabIndex={0}>
						{value}
					</div>
				)}
			</>
		);

	return (
		<div className={classes}>
			{async && renderAsyncButton()}
			{!async && renderButton()}
		</div>
	);
};

export default Button;
