import React from 'react';

import './style.scss';

const ErrorMessage = props => {
	const { error } = props;

	return <div className="error-message">{error}</div>;
};

export default ErrorMessage;
