import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { notificationsSelector } from 'state/notifications/notifications.selectors';

import { 
	addNotification as addNotificationSliceAction,
	removeNotification as removeNotificationSliceAction,
	NotificationLevel 
} from 'state/notifications/notifications.reducer';

import store from 'store';

import './style.css';

export const NotificationSystemStore = {
	addNotification: notification =>
		store.dispatch(addNotificationSliceAction(notification)),
	removeNotification: notification =>
		store.dispatch(removeNotificationSliceAction(notification))
};

export const Notification = props => {
	const [isVisible, setIsVisible] = useState(false);
	const [isDismissed, setIsDismissed] = useState(false);

	const { level, message, onClose, stayVisible = false, title } = props;

	const handleClose = () => {
		setIsDismissed(true);

		setTimeout(() => {
			if (onClose) onClose(props);
		}, 300);
	};

	useEffect(() => {
		setTimeout(() => setIsVisible(true), 100);
	}, []);

	useEffect(() => {
		if (isVisible && !stayVisible) {
			setTimeout(() => handleClose(), 6000);
		}
	}, [isVisible]);

	const renderDebug = () => (
		<div className="content">
			<FontAwesomeIcon icon="bug" />
			<div className="content-column">
				{title && <h5 className="title">{title}</h5>}
				<p className={`message ${level}`}>{message}</p>
			</div>
		</div>
	);
	const renderInfo = () => (
		<div className="content">
			<FontAwesomeIcon icon="info-circle" />
			<div className="content-column">
				{title && <h5 className="title">{title}</h5>}
				<p className={`message ${level}`}>{message}</p>
			</div>
		</div>
	);
	const renderSuccess = () => (
		<div className="content">
			<FontAwesomeIcon icon="check-circle" />
			<div className="content-column">
				{title && <h5 className="title">{title}</h5>}
				<p className={`message ${level}`}>{message}</p>
			</div>
		</div>
	);
	const renderWarning = () => (
		<div className="content">
			<FontAwesomeIcon icon="exclamation-circle" />
			<div className="content-column">
				{title && <h5 className="title">{title}</h5>}
				<p className={`message ${level}`}>{message}</p>
			</div>
		</div>
	);
	return (
		<div
			className={`notification ${level} ${isVisible ? 'visible' : ''} ${
				isDismissed ? 'dismissed' : ''
			}`}>
			{level && level === 'warn' && renderWarning()}
			{level && level === 'debug' && renderDebug()}
			{level && level === 'info' && renderInfo()}
			{level && level === 'success' && renderSuccess()}
			<FontAwesomeIcon icon="times" onClick={handleClose} />
		</div>
	);
};

export const NotificationSystem = () => {
	const { notifications } = useSelector(state => notificationsSelector(state));

	return (
		<div className="notification-system">
			{notifications &&
				notifications.length > 0 &&
				notifications.map(n => (
					<Notification
						key={n.id}
						id={n.id}
						level={n.level}
						message={n.message}
						onClose={notification => {
							NotificationSystemStore.removeNotification(notification);
							if (n.onClose) n.onClose(notification);
						}}
						stayVisible={n.stayVisible || n.level === NotificationLevel.WARN}
						title={n.title}
					/>
				))}
		</div>
	);
};
