import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    all: [],
    current: undefined,
    isBusy: false
};

const campaignsSlice = createSlice({
    name: 'campaigns',
    initialState,
    reducers: {
        saveCampaign(state) {
            state.isBusy = true;
        },
        setCampaigns(state, action) {
            state.all = action.payload.campaigns;
            state.isBusy = false;
        },
        saveCampaignError(state) {
            state.isBusy = false;
        },
        setCurrentCampaign(state, action) {
            state.current = action.payload.campaign;
        }
    }
});

export const { saveCampaign, saveCampaignError, setCampaigns, setCurrentCampaign } = campaignsSlice.actions;

export default campaignsSlice.reducer;

// /**
//  * ACTION TYPE CONSTANTS
//  */
// export const actionTypes = {
//     ADD_PLAYER: 'campaigns/ADD_PLAYER',
//     GET_CAMPAIGNS: 'campaigns/GET_CAMPAIGNS',
//     GET_CAMPAIGNS_ERROR: 'campaigns/GET_CAMPAIGNS_ERROR',
//     GET_CAMPAIGN: 'campaigns/GET_CAMPAIGN',
//     GET_CAMPAIGN_ERROR: 'campaigns/GET_CAMPAIGN_ERROR',
//     SET_CAMPAIGNS: 'campaigns/SET_CAMPAIGNS',
//     SAVE_CAMPAIGN: 'campaigns/SAVE_CAMPAIGN',
//     SAVE_CAMPAIGN_ERROR: 'campaigns/SAVE_CAMPAIGN_ERROR',
//     SET_CURRENT_CAMPAIGN: 'campaigns/SET_CURRENT_CAMPAIGN'
// };

// /**
//  * ACTIONS
//  */
// export const actions = {
//     addPlayer: (payload) => ({
//         type: actionTypes.ADD_PLAYER,
//         payload
//     }),
//     getAll: () => ({
//         type: actionTypes.GET_CAMPAIGNS
//     }),
//     get: (payload) => ({
//         type: actionTypes.GET_CAMPAIGN,
//         payload
//     }),
//     save: (payload) => ({
//         type: actionTypes.SAVE_CAMPAIGN,
//         payload
//     }),
//     setCurrentCampaign: (payload) => ({
//         type: actionTypes.SET_CURRENT_CAMPAIGN,
//         payload
//     })
// };

// /**
//  * REDUCER (default export)
//  */
// const initialState = {
//     all: [],
//     current: undefined,
//     isBusy: false
// };
// export default function campaignsReducer(state = initialState, action) {
//     const nextState = { ...state };
//     switch (action.type) {
//         case actionTypes.SAVE_CAMPAIGN: {
//             return { ...nextState, isBusy: true };
//         }
//         case actionTypes.SET_CAMPAIGNS: {
//             return { ...nextState, all: action.payload.campaigns, isBusy: false };
//         }
//         case actionTypes.SAVE_CAMPAIGN_ERROR: {
//             return { ...nextState, isBusy: false };
//         }
//         case actionTypes.SET_CURRENT_CAMPAIGN: {
//             return { ...nextState, current: action.payload.campaign };
//         }
//         default:
//             return state;
//     }
// }
