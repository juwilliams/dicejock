import { put, takeLatest } from 'redux-saga/effects';

import CampaignService from 'services/CampaignService';

import {
    addPlayerAction,
    getCampaignAction,
    getCampaignsAction,
    getCampaignErrorAction
} from 'state/campaigns/campaigns.actions';

import { saveCampaign, setCurrentCampaign, setCampaigns, saveCampaignError } from 'state/campaigns/campaigns.reducer';

import { baseEffectHandler } from 'util/sagas';

import { NotificationSystemStore } from 'ui-kit';
import { NotificationLevel } from 'state/notifications/notifications.reducer';

export default function* campaignSaga() {
    yield takeLatest(addPlayerAction.TRIGGER, (action) =>
        baseEffectHandler({
            service: CampaignService.addPlayer,
            data: action.payload,
            *onResponse() {
                yield put(getCampaignsAction());
            }
        })
    );
    yield takeLatest(getCampaignsAction.TRIGGER, () =>
        baseEffectHandler({
            service: CampaignService.getAll,
            *onResponse(data) {
                yield put(setCampaigns(data));
            }
        })
    );
    yield takeLatest(getCampaignAction.TRIGGER, (action) =>
        baseEffectHandler({
            service: CampaignService.get,
            data: action.payload,
            *onResponse(data) {
                yield put(setCurrentCampaign(data));
            },
            *onError() {
                yield put(getCampaignErrorAction());
                NotificationSystemStore.addNotification({
                    message: 'An error was encountered while fetching this campaign. Try again later.',
                    level: NotificationLevel.WARN,
                    title: 'Campaigns',
                    stayVisible: true
                });
            }
        })
    );
    yield takeLatest(saveCampaign.type, (action) =>
        baseEffectHandler({
            service: CampaignService.save,
            data: action.payload,
            *onResponse() {
                yield put(getCampaignsAction());
                NotificationSystemStore.addNotification({
                    message: 'Campaign Saved',
                    level: NotificationLevel.SUCCESS,
                    title: 'Campaigns'
                });
            },
            *onError() {
                yield put(saveCampaignError());
                NotificationSystemStore.addNotification({
                    message: 'Save unsuccesful. Try again later.',
                    level: NotificationLevel.WARN,
                    title: 'Campaigns',
                    stayVisible: true
                });
            }
        })
    );
}
