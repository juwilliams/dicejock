import { createRoutine } from 'redux-saga-routines';

export const addPlayerAction = createRoutine('CAMPAIGNS_ADD_PLAYER');
export const getPlayersAction = createRoutine('CAMPAIGNS_GET_PLAYERS');
export const getCampaignErrorAction = createRoutine('CAMPAIGNS_GET_FAILURE');
export const getCampaignsAction = createRoutine('CAMPAIGNS_GET_ALL');
export const getCampaignAction = createRoutine('CAMPAIGNS_GET');
