import { createSelector } from '@reduxjs/toolkit';

const campaignsState = (state) => state.campaignsReducer;
const campaignId = (state, id) => id;

export const campaignsSelector = createSelector(campaignsState, (state) => state);

export const currentCampaignSelector = createSelector([campaignsSelector], ({ current }) => current);
export const campaignByIdSelector = createSelector([campaignsSelector, campaignId], ({ all }, id) =>
    all.find((camp) => camp.id === id)
);
