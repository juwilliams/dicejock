import { createSelector } from '@reduxjs/toolkit';

const authState = (state) => state.authReducer;

export const authSelector = createSelector(authState, (state) => state);

export const authTokenSelector = createSelector([authSelector], ({ token }) => ({
    token
}));
