import { call, put, takeLatest } from 'redux-saga/effects';
import { NotificationSystemStore } from 'ui-kit';
import { NotificationLevel } from 'state/notifications/notifications.reducer';
import { login as loginSliceAction, loginSuccess, loginFailure, userLoggedOut } from 'state/auth/auth.reducer';
import { logoutAction, registerAction } from 'state/auth/auth.actions';

import AuthService from '../../services/AuthService';

function* login(action) {
    try {
        const { email, password, captchaToken } = action.payload;
        const result = yield call(AuthService.login, email, password, captchaToken);
        if (!result.error) {
            yield put(loginSuccess(result));
        } else {
            throw result.error;
        }
    } catch (err) {
        yield put(
            loginFailure({
                error:
                    'Looks like something went wrong trying to log into your account. Check your username and password and try again.'
            })
        );
    }
}

function* logout() {
    try {
        yield put(userLoggedOut());
        NotificationSystemStore.addNotification({
            message: 'You have been logged out.',
            level: NotificationLevel.INFO,
            title: 'Account'
        });
    } catch (err) {
        console.log(err);
    }
}

function* register(action) {
    try {
        const { username, email, password, captchaToken } = action.payload;
        const result = yield call(AuthService.register, username, email, password, captchaToken);
        if (!result.error) {
            yield put(loginSuccess(result));
        } else {
            throw result.error;
        }
    } catch (err) {
        yield put(
            loginFailure({
                error: 'Looks like something went wrong trying to register your account. Give it another try.'
            })
        );
    }
}

export default function* authSaga() {
    yield takeLatest(loginSliceAction.type, login);
    yield takeLatest(logoutAction.TRIGGER, logout);
    yield takeLatest(registerAction.TRIGGER, register);
}
