import { createSlice } from '@reduxjs/toolkit';

import { setDefaultHeader } from 'util/axiosClient';
import storageHelper from 'util/storageHelper';

const initialState = {
    initComplete: false,
    error: undefined,
    isAuthenticating: false,
    token: storageHelper.local.getAuthToken()
};

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        init(state) {
            const { token } = state;
            if (token) setDefaultHeader('Authorization', `Bearer ${token}`);
            state.initComplete = true;
        },
        login(state) {
            state.error = undefined;
            state.isAuthenticating = true;
        },
        loginSuccess(state, action) {
            const { token } = action.payload;

            state.token = token;
            state.isAuthenticating = false;

            setDefaultHeader('Authorization', token);

            storageHelper.local.setAuthToken(token);
        },
        userLoggedOut(state) {
            state.token = null;
            state.isAuthenticating = false;

            storageHelper.local.removeAuthToken();
        },
        loginFailure(state, action) {
            state.error = action.payload.error;
            state.isAuthenticating = false;
        },
        addConnection(state, action) {
            state.connections = [
                ...state.connections.filter((c) => c.filePath !== action.payload.filePath),
                action.payload
            ];
        },
        removeConnection(state, action) {
            state.connections = [...state.connections.filter((c) => c.filePath !== action.payload.filePath)];
        }
    }
});

export const { init, login, loginSuccess, userLoggedOut, loginFailure } = authSlice.actions;

export default authSlice.reducer;
