import { createRoutine } from 'redux-saga-routines';

export const logoutAction = createRoutine('AUTH_LOGOUT');
export const registerAction = createRoutine('AUTH_REGISTER');
