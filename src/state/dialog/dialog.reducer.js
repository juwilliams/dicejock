import { createSlice } from '@reduxjs/toolkit';
import { v4 as uuid } from 'uuid';

export const DialogTypes = {
    ATTRIBUTE: 'attribute',
    CHARACTER: 'character',
    FEAT: 'feat',
    SKILL: 'skill'
};

const initialState = { current: undefined };

const dialogSlice = createSlice({
    name: 'dialog',
    initialState,
    reducers: {
        showDialog(state, action) {
            const nextDialog = action.payload;
            nextDialog.id = uuid();
            state.current = action.payload;
        },
        hideDialog(state) {
            state.current = undefined;
        }
    }
});

export const { showDialog, hideDialog } = dialogSlice.actions;

export default dialogSlice.reducer;
