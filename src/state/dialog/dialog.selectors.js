import { createSelector } from '@reduxjs/toolkit';

const dialogState = (state) => state.dialogReducer;

export const dialogSelector = createSelector(dialogState, (state) => state);
