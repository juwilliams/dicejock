import { createRoutine } from 'redux-saga-routines';

export const getTemplateAction = createRoutine('TEMPLATES_GET');
export const getTemplatesAction = createRoutine('TEMPLATES_GET_ALL');
