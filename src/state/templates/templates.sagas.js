import { put, takeLatest } from 'redux-saga/effects';

import TemplateService from 'services/TemplateService';

import { getTemplateAction, getTemplatesAction } from 'state/templates/templates.actions';
import { saveTemplate, saveTemplateError, setCurrentTemplate, setTemplates } from 'state/templates/templates.reducer';

import { baseEffectHandler } from 'util/sagas';

import { NotificationSystemStore } from 'ui-kit';
import { NotificationLevel } from 'state/notifications/notifications.reducer';

export default function* templateSaga() {
    yield takeLatest(getTemplatesAction.TRIGGER, () =>
        baseEffectHandler({
            service: TemplateService.getAll,
            *onResponse(data) {
                yield put(setTemplates(data));
            },
            *onError() {
                yield put(getTemplatesAction.failure());
                NotificationSystemStore.addNotification({
                    message: 'Error encountered while fetching templates. Try again later.',
                    level: NotificationLevel.WARN,
                    title: 'Templates',
                    stayVisible: true
                });
            }
        })
    );
    yield takeLatest(getTemplateAction.TRIGGER, (action) =>
        baseEffectHandler({
            service: TemplateService.get,
            data: action.payload,
            *onResponse(data) {
                yield put(setCurrentTemplate(data));
            },
            *onError() {
                yield put(getTemplateAction.failure());
                NotificationSystemStore.addNotification({
                    message: 'Error encountered while fetching this template. Try again later.',
                    level: NotificationLevel.WARN,
                    title: 'Templates',
                    stayVisible: true
                });
            }
        })
    );
    yield takeLatest(saveTemplate.type, (action) =>
        baseEffectHandler({
            service: TemplateService.save,
            data: action.payload,
            *onResponse() {
                yield put(getTemplatesAction());
                NotificationSystemStore.addNotification({
                    message: 'Template Saved',
                    level: NotificationLevel.SUCCESS,
                    title: 'Templates'
                });
            },
            *onError() {
                yield put(saveTemplateError());
                NotificationSystemStore.addNotification({
                    message: 'Save unsuccesful. Try again later.',
                    level: NotificationLevel.WARN,
                    title: 'Templates',
                    stayVisible: true
                });
            }
        })
    );
}
