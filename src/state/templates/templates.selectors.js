import { createSelector } from '@reduxjs/toolkit';

import { templateTypes } from 'schema/template';

const templatesState = (state) => state.templatesReducer;

export const templatesSelector = createSelector(templatesState, (state) => state);

export const gameTemplatesSelector = createSelector(
    [templatesSelector],
    ({ all }) => all?.filter((template) => template.type === templateTypes.GAME) || []
);
export const characterTemplatesSelector = createSelector(
    [templatesSelector],
    ({ all }) => all?.filter((template) => template.type === templateTypes.CHARACTER) || []
);
export const itemTemplatesSelector = createSelector(
    [templatesSelector],
    ({ all }) => all?.filter((template) => template.type === templateTypes.ITEM) || []
);
