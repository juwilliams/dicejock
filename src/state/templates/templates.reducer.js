import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    all: [],
    current: undefined,
    isBusy: false
};
const templatesSlice = createSlice({
    name: 'templates',
    initialState,
    reducers: {
        saveTemplate(state) {
            state.isBusy = true;
        },
        saveTemplateError(state) {
            state.isBusy = false;
        },
        setTemplates(state, action) {
            state.all = action.payload.templates;
            state.isBusy = false;
        },
        setCurrentTemplate(state, action) {
            state.current = action.payload.template;
        }
    }
});

export const { saveTemplate, saveTemplateError, setTemplates, setCurrentTemplate } = templatesSlice.actions;

export default templatesSlice.reducer;
