import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    all: [],
    current: undefined,
    isBusy: false
};

const playersSlice = createSlice({
    name: 'players',
    initialState,
    reducers: {
        savePlayer(state) {
            state.isBusy = true;
        },
        savePlayerError(state) {
            state.isBusy = false;
        },
        setPlayers(state, action) {
            state.all = action.payload.players;
            state.isBusy = false;
        },
        setCurrentPlayer(state, action) {
            state.current = action.payload.player;
        }
    }
});

export const { savePlayer, savePlayerError, setPlayers, setCurrentPlayer } = playersSlice.actions;

export default playersSlice.reducer;
