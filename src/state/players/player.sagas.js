import { put, takeLatest } from 'redux-saga/effects';

import PlayerService from 'services/PlayerService';

import { getPlayersAction } from 'state/campaigns/campaigns.actions';
import { savePlayer, savePlayerError, setPlayers } from 'state/players/players.reducer';

import { baseEffectHandler } from 'util/sagas';

import { NotificationSystemStore } from 'ui-kit';
import { NotificationLevel } from 'state/notifications/notifications.reducer';

export default function* playersSaga() {
    yield takeLatest(getPlayersAction.TRIGGER, () =>
        baseEffectHandler({
            service: PlayerService.getAll,
            *onResponse(data) {
                yield put(setPlayers(data));
            }
        })
    );
    yield takeLatest(savePlayer.type, (action) =>
        baseEffectHandler({
            service: PlayerService.save,
            data: action.payload,
            *onResponse() {
                yield put(getPlayersAction());
                NotificationSystemStore.addNotification({
                    message: 'Player Saved',
                    level: NotificationLevel.SUCCESS,
                    title: 'Players'
                });
            },
            *onError() {
                yield put(savePlayerError());
                NotificationSystemStore.addNotification({
                    message: 'Save unsuccesful. Try again later.',
                    level: NotificationLevel.WARN,
                    title: 'Players',
                    stayVisible: true
                });
            }
        })
    );
}
