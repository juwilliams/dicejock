import { createSelector } from '@reduxjs/toolkit';

const playersState = (state) => state.playersReducer;

export const playersSelector = createSelector(playersState, (state) => state);
