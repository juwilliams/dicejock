import { createRoutine } from 'redux-saga-routines';

export const getPlayersAction = createRoutine('PLAYERS_GET_ALL');
