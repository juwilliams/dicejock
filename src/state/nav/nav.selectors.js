import { createSelector } from '@reduxjs/toolkit';

const navState = (state) => state.navReducer;

export const navSelector = createSelector(navState, (state) => state);
