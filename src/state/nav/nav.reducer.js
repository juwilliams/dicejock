import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    initialPath: undefined
};

const navSlice = createSlice({
    name: 'nav',
    initialState,
    reducers: {
        setInitialPath(state, action) {
            state.initialPath = action.payload;
        }
    }
});

export const { setInitialPath } = navSlice.actions;

export default navSlice.reducer;
