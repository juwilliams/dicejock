import { createSlice } from '@reduxjs/toolkit';
import { v4 as uuid } from 'uuid';

export const NotificationLevel = {
    DEBUG: 'debug',
    INFO: 'info',
    SUCCESS: 'success',
    WARN: 'warn'
};

const initialState = { initComplete: false, notifications: [] };

const notificationSlice = createSlice({
    name: 'notifications',
    initialState,
    reducers: {
        addNotification(state, action) {
            const nextNotification = action.payload;
            nextNotification.id = uuid();

            if (nextNotification.error) {
                state.errors = [...state.errors, nextNotification.error];
            }

            state.notifications = [...state.notifications, nextNotification];
        },
        removeNotification(state, action) {
            state.notifications = state.notifications.filter((n) => n.id && n.id !== action.payload.id);
        }
    }
});

export const { addNotification, removeNotification } = notificationSlice.actions;

export default notificationSlice.reducer;
