import { createSelector } from '@reduxjs/toolkit';

const notificationsState = (state) => state.notificationsReducer;

export const notificationsSelector = createSelector(notificationsState, (state) => state);
