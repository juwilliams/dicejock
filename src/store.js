import { createLogger } from 'redux-logger';
import { configureStore, combineReducers } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';

import authSaga from 'state/auth/auth.sagas';
import campaignsSaga from 'state/campaigns/campaigns.sagas';
import playersSaga from 'state/players/player.sagas';
import templatesSaga from 'state/templates/templates.sagas';

import authReducer from 'state/auth/auth.reducer';
import dialogReducer from 'state/dialog/dialog.reducer';
import campaignsReducer from 'state/campaigns/campaigns.reducer';
import navReducer from 'state/nav/nav.reducer';
import notificationsReducer from 'state/notifications/notifications.reducer';
import playersReducer from 'state/players/players.reducer';
import templatesReducer from 'state/templates/templates.reducer';

const logger = createLogger({ collapsed: true });

const sagaMiddleware = createSagaMiddleware();

const sagas = [authSaga, campaignsSaga, playersSaga, templatesSaga];
const reducers = {
    authReducer,
    dialogReducer,
    campaignsReducer,
    navReducer,
    notificationsReducer,
    playersReducer,
    templatesReducer
};

const store = configureStore({
    reducer: combineReducers(reducers),
    middleware: [logger, sagaMiddleware]
});

sagas.map(sagaMiddleware.run);

export default store;
