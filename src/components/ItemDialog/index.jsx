import React, { useEffect, useMemo, useState } from 'react';

import {
	Button,
	Dialog,
	Label,
	PageRow,
	PageColumn,
	Select,
	TextField
} from 'ui-kit';

const ItemDialog = props => {
    const { open, edit, onAdd, onClose, item, itemTemplates } = props;
    
	const [name, setName] = useState(item?.name || '');
    const [description, setDescription] = useState(item?.description || '');
    const [attributes, setAttributes] = useState(undefined);
	
    //  options state
    const [itemTemplateOptions, setItemTemplateOptions] = useState([]);
    
	//  select state
	const [selectedItemTemplate, setSelectedItemTemplate] = useState(undefined);
    
    useMemo(() => {
		setItemTemplateOptions(
			itemTemplates.map(template => ({
				key: template.name,
				label: template.name,
				value: template.name
			}))
		);
	}, [item, itemTemplates]);

	useEffect(() => {
		setName(item?.name || '');
		setDescription(item?.description || '');
		setSelectedItemTemplate(itemTemplates?.find(template => template.name === item?.templateName));
	}, [item, itemTemplateOptions]);

    useEffect(() => {
		//	if this is the attribute currently assigned, we need to populate the attribute with the value
		setAttributes(selectedItemTemplate?.attributes?.map(attr => {
			const existingCharAttribute = item?.attributes?.find(charAttr => charAttr.id === attr.id);
			return {
				...attr,
				value: existingCharAttribute?.value
			};
		}));
            // setAttributes(selectedItemTemplate.attributes?.map(attr => ({ id: attr.id, name: attr.name, ...attr})));
    }, [selectedItemTemplate]);

    const handleAttributeChange = ({ id, name: attributeName, value }) => {
        setAttributes([
            ...attributes.filter(attr => attr.name !== attributeName),
            { id, name: attributeName, value }
        ]);
    }
    const handleAdd = () => {
        if (onAdd) {
            if (onAdd({
                name, description, attributes: attributes.map(attr => ({ id: attr.id, name: attr.name, value: attr.value })), templateName: selectedItemTemplate?.name, templateId: selectedItemTemplate?.id
            })) {
				onClose();
			}
        }
    }
    
	return (
		<Dialog
			open={open}
			onClose={onClose}
			actions={[
				<Button
					key="rule-dialog-actions-submit"
					value={edit ? 'Edit' : 'Add'}
					onClick={handleAdd}
					primary
				/>
			]}>
			<Label value="item Configuration" />
			<PageRow nopadding>
				<PageColumn>
					<TextField
						label="Name"
						onChange={e => setName(e.target.value)}
						defaultValue={name}
					/>
                    {selectedItemTemplate && selectedItemTemplate.attributes?.map(attr => (
                        <TextField key={`item-attr-${attr.id}`} defaultValue={item?.attributes.find(charAttr => charAttr.id === attr.id)?.value} label={attr.name} placeholder={`${attr.type} Min(${attr.minValue}) Max(${attr.maxValue})`} onChange={e => handleAttributeChange({ value: e.target.value, ...attr })} />
                    ))}
				</PageColumn>
				<PageColumn>
					<Select
						label="Template"
						options={itemTemplateOptions}
						onChange={opt => setSelectedItemTemplate(itemTemplates.find(temp => temp.name === opt.value))}
						defaultValue={itemTemplateOptions.find(opt => opt.value === selectedItemTemplate?.name)}
					/>
					<TextField
						label="Description"
						multiline
						rows={5}
						onChange={e => setDescription(e.target.value)}
						defaultValue={description}
					/>
				</PageColumn>
			</PageRow>
		</Dialog>
	);
};

export default ItemDialog;
