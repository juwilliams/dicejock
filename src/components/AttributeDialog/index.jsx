import React, { useEffect, useState } from 'react';

import {
	Button,
	Dialog,
	Label,
	PageRow,
	PageColumn,
	Select,
	TextField
} from 'ui-kit';

import {
	attributeTypeOptions,
	attributeAvailableToOptions
} from 'schema/attribute';

const AttributeDialog = props => {
	const { open, edit, onAdd, onClose, attribute } = props;
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [minValue, setMinValue] = useState('');
	const [maxValue, setMaxValue] = useState('');
	
	//  select state
	const [selectedType, setSelectedType] = useState(undefined);
	const [selectedAvailableTo, setSelectedAvailableTo] = useState(undefined);

	useEffect(() => {
		setName(attribute?.name || '');
		setDescription(attribute?.description || '');
		setMinValue(attribute?.minValue || 0);
		setMaxValue(attribute?.maxValue || 0);
		setSelectedType(
			attribute?.type
				? attributeTypeOptions.find(o => o.value === attribute.type)
				: undefined || undefined
		);
		setSelectedAvailableTo(
			attribute?.availableTo
				? attributeAvailableToOptions.find(o => o.value === attribute.availableTo)
				: undefined || undefined
		);
	}, [attribute]);

	const handleAddClick = () => {
		if (onAdd) {
			const outAttribute = {
				id: attribute?.id,
				name,
				description,
				minValue,
				maxValue,
				availableTo: selectedAvailableTo ? selectedAvailableTo.value : undefined,
				type: selectedType ? selectedType.value : undefined
			};
			if (onAdd({ attribute: outAttribute, edit })) onClose();
		}
	};
	
	return (
		<Dialog
			open={open}
			onClose={onClose}
			actions={[
				<Button
					key="attribute-dialog-submit"
					value={edit ? 'Edit' : 'Add'}
					onClick={handleAddClick}
					primary
				/>
			]}>
			<Label value="Attribute Configuration" />
			<PageRow nopadding>
				<PageColumn>
					<TextField
						label="Name"
						onChange={e => setName(e.target.value)}
						defaultValue={name}
					/>
					<Select
						label="Type"
						options={attributeTypeOptions}
						onChange={opt => setSelectedType(opt)}
						defaultValue={selectedType}
					/>
					<Select
						label="Available To"
						options={attributeAvailableToOptions}
						onChange={opt => setSelectedAvailableTo(opt)}
						defaultValue={selectedAvailableTo}
					/>
				</PageColumn>
				<PageColumn>
					<TextField
						label="Min Value"
						onChange={e => setMinValue(e.target.value)}
						defaultValue={minValue}
					/>
					<TextField
						label="MaxValue"
						onChange={e => setMaxValue(e.target.value)}
						defaultValue={maxValue}
					/>
					<TextField
						label="Description"
						multiline
						rows={5}
						onChange={e => setDescription(e.target.value)}
						defaultValue={description}
					/>
				</PageColumn>
			</PageRow>
		</Dialog>
	);
};

export default AttributeDialog;
