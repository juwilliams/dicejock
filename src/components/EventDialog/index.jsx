import React, { useMemo, useState } from 'react';

import {
	Button,
    Dialog,
    Grid,
	Label,
	PageRow,
    PageColumn,
    PageInnard,
	Select,
	TextField
} from 'ui-kit';

import { EventTriggerOptions } from 'schema/event';
import { characterGridHeaders } from 'schema/character';
import { itemGridHeaders } from 'schema/item';

const EventDialog = props => {
    const { open, edit, onAdd, onClose, event, characters, items } = props;
    
	const [name, setName] = useState(event?.name || '');
	const [location, setLocation] = useState(event?.location || '');
    const [description, setDescription] = useState(event?.description || '');
    const [eventCharacters, setEventCharacters] = useState(event?.characters);
    const [eventItems, setEventItems] = useState(event?.items);
    
    //  options state
    const [characterOptions, setCharacterOptions] = useState([]);
    const [itemOptions, setItemOptions] = useState([]);
    
	//  select state
    const [selectedCharacter, setSelectedCharacter] = useState(undefined);
	const [selectedEventTrigger, setSelectedEventTrigger] = useState(undefined);
    const [selectedEventCharacters, setSelectedEventCharacters] = useState(undefined);

    const [selectedItem, setSelectedItem] = useState(undefined);
    const [selectedEventItems, setSelectedEventItems] = useState(undefined);

    useMemo(() => {
		setCharacterOptions(
			characters?.map(c => ({
				key: c.name,
				label: c.name,
				value: c.name
			}))
        );
    }, [event, characters]);
    
    useMemo(() => {
        setItemOptions(
            items?.map(i => ({
                key: i.name,
                label: i.name,
                value: i.name
            }))
        );
    }, [event, items]);

    //  characters
    const handleAddSelectedCharacter = () => {
        if (!selectedCharacter) return;
        setEventCharacters([
            ...eventCharacters?.filter(r => r.name !== selectedCharacter?.name) || [],
            selectedCharacter
        ]);
    }
    const handleRemoveSelectedCharacters = () => {
        if (!selectedEventCharacters) return;

        setEventCharacters([
            ...eventCharacters?.filter(r => !selectedEventCharacters?.find(s => s.name === r.name))
        ]);
    }
    
    // items
    const handleAddSelectedItem = () => {
        if (!selectedItem) return;
        setEventItems([
            ...eventItems?.filter(r => r.name !== selectedItem?.name) || [],
            selectedItem
        ]);
    }
    const handleRemoveSelectedItems = () => {
        if (!selectedEventItems) return;

        setEventCharacters([
            ...eventItems?.filter(r => !selectedEventItems?.find(s => s.name === r.name))
        ]);
    }
    
    const handleAdd = () => {
        if (onAdd) {
            if (onAdd({
                name, description, location, trigger: selectedEventTrigger, characters: eventCharacters, items: eventItems
            })) {
				onClose();
			}
        }
    }
    
	return (
		<Dialog
			open={open}
			onClose={onClose}
			actions={[
				<Button
					key="rule-dialog-actions-submit"
					value={edit ? 'Edit' : 'Add'}
					onClick={handleAdd}
					primary
				/>
			]}>
			<Label value="Event Configuration" />
			<PageRow nopadding fullwidth>
				<PageColumn fullwidth>
					<TextField
						label="Name"
						onChange={e => setName(e.target.value)}
						defaultValue={name}
					/>
					<TextField
						label="Location"
						onChange={e => setLocation(e.target.value)}
						defaultValue={location}
					/>
					<Select
						label="Event Trigger"
						options={EventTriggerOptions}
						onChange={opt => setSelectedEventTrigger(opt.value)}
						defaultValue={EventTriggerOptions.find(opt => opt.value === event?.trigger)}
					/>
					<TextField
						label="Description"
						multiline
						rows={5}
						onChange={e => setDescription(e.target.value)}
						defaultValue={description}
					/>
				</PageColumn>
			</PageRow>
            <PageRow>
                <PageColumn nopadding>
                    <PageInnard nopadding>
                        <Grid
                            actions={
                                <>
                                    <Select
                                        options={characterOptions}
                                        onChange={opt => setSelectedCharacter(characters?.find(c => c.name === opt.value))}
                                    />
                                    <Button
                                        value="Add Character"
                                        primary
                                        small
                                        onClick={handleAddSelectedCharacter}
                                    />
                                    <Button
                                        value="Remove Characters"
                                        primary
                                        small
                                        onClick={handleRemoveSelectedCharacters}
                                    />
                                </>
                            }
                            fullwidth
                            onSelection={selection => setSelectedEventCharacters(selection)}
                            multipleSelection
                            title="Characters"
                            headers={characterGridHeaders}
                            data={eventCharacters}
                        />
                    </PageInnard>
                </PageColumn>
            </PageRow>
            <PageRow fullwidth>
                <PageColumn nopadding fullwidth>
                    <PageInnard nopadding>
                        <Grid
                            actions={
                                <>
                                    <Select
                                        options={itemOptions}
                                        onChange={opt => setSelectedItem(items?.find(c => c.name === opt.value))}
                                    />
                                    <Button
                                        value="Add Item"
                                        primary
                                        small
                                        onClick={handleAddSelectedItem}
                                    />
                                    <Button
                                        value="Remove Items"
                                        primary
                                        small
                                        onClick={handleRemoveSelectedItems}
                                    />
                                </>
                            }
                            fullwidth
                            onSelection={selection => setSelectedEventItems(selection)}
                            multipleSelection
                            title="Items"
                            headers={itemGridHeaders}
                            data={eventItems}
                        />
                    </PageInnard>
                </PageColumn>
            </PageRow>
		</Dialog>
	);
};

export default EventDialog;
