import React from 'react';
import { withRouter } from 'react-router-dom';

import { LinkButton } from 'ui-kit';

import './style.css';

const SiteNav = props => {
	const {
		isLoggedIn,
		match: { path }
	} = props;

	return (
		<div className="site-nav">
			<h5 className="site-title">diceboards</h5>
			<div className="nav-items">
				{!isLoggedIn && (
					<>
						<LinkButton to="/about" isActive={path === '/about'}>
							About
						</LinkButton>
						<LinkButton to="/features" isActive={path === '/features'}>
							Features
						</LinkButton>
						<LinkButton to="/get-started" isActive={path === '/get-started'}>
							Get Started
						</LinkButton>
						<LinkButton to="/login" isActive={path === '/login'}>
							Log in
						</LinkButton>
						<LinkButton to="/register" primary>
							Create an account
						</LinkButton>
					</>
				)}
				{isLoggedIn && (
					<>
						<LinkButton to="/journal" isActive={path === '/journal'}>
							Journal
						</LinkButton>
						<LinkButton to="/designer" isActive={path === '/designer'}>
							Designer
						</LinkButton>
						<LinkButton to="/logout" isActive={path === '/about'}>
							Logout
						</LinkButton>
					</>
				)}
			</div>
		</div>
	);
};

export default withRouter(SiteNav);
