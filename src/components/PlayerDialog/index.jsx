import React from 'react';

import { Dialog } from 'ui-kit';

const PlayerDialog = props => {
	const { open, onClose } = props;
	return <Dialog open={open} onClose={onClose} />;
};

export default PlayerDialog;
