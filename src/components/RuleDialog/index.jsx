import React, { useEffect, useState } from 'react';

import {
	Button,
	Dialog,
	Label,
	PageRow,
	PageColumn,
	Select,
	TextField
} from 'ui-kit';

import {
	ruleTypeOptions,
	ruleAffectOptions,
	ruleTriggerOptions
} from 'schema/rule';

const RuleDialog = props => {
	const { open, edit, onAdd, onClose, rule } = props;
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [value, setValue] = useState('');

	//  select state
	const [selectedType, setSelectedType] = useState(undefined);
	const [selectedTrigger, setSelectedTrigger] = useState(undefined);
	const [selectedAffect, setSelectedAffect] = useState(undefined);
	const [selectedTarget, setSelectedTarget] = useState(undefined);

	useEffect(() => {
		setName(rule?.name || '');
		setDescription(rule?.description || '');
		setValue(rule?.value || '');
		setSelectedType(
			rule?.type
				? ruleTypeOptions.find(o => o.value === rule.type)
				: undefined || undefined
		);
		setSelectedTrigger(
			rule?.trigger
				? ruleTriggerOptions.find(o => o.value === rule.trigger)
				: undefined || undefined
		);
		setSelectedAffect(
			rule?.affect
				? ruleAffectOptions.find(o => o.value === rule.affect)
				: undefined || undefined
		);
	}, [rule]);

	const handleAddClick = () => {
		if (onAdd) {
			const outRule = {
				id: rule?.id,
				name,
				description,
				value,
				type: selectedType ? selectedType.value : undefined,
				trigger: selectedTrigger ? selectedTrigger.value : undefined,
				affect: selectedAffect ? selectedAffect.value : undefined,
				target: selectedTarget ? selectedTarget.value : undefined
			};
			if (onAdd({ rule: outRule, edit })) onClose();
		}
	};

	return (
		<Dialog
			open={open}
			onClose={onClose}
			actions={[
				<Button
					key="rule-dialog-actions-submit"
					value={edit ? 'Edit' : 'Add'}
					onClick={handleAddClick}
					primary
				/>
			]}>
			<Label value="Rule Configuration" />
			<PageRow nopadding>
				<PageColumn>
					<TextField
						label="Name"
						onChange={e => setName(e.target.value)}
						defaultValue={name}
					/>
					<Select
						label="Type"
						options={ruleTypeOptions}
						onChange={opt => setSelectedType(opt)}
						defaultValue={selectedType}
					/>
					<Select
						label="Trigger"
						options={ruleTriggerOptions}
						onChange={opt => setSelectedTrigger(opt)}
						defaultValue={selectedTrigger}
					/>
					<Select
						label="Affects"
						options={ruleAffectOptions}
						onChange={opt => setSelectedAffect(opt)}
						defaultValue={selectedAffect}
					/>
				</PageColumn>
				<PageColumn>
					<Select
						label="Target"
						options={[]}
						onChange={opt => setSelectedTarget(opt)}
						defaultValue={selectedTarget}
					/>
					<TextField
						label="Value"
						onChange={e => setValue(e.target.value)}
						defaultValue={value}
					/>
					<TextField
						label="Description"
						multiline
						rows={5}
						onChange={e => setDescription(e.target.value)}
						defaultValue={description}
					/>
				</PageColumn>
			</PageRow>
		</Dialog>
	);
};

export default RuleDialog;
