import React from 'react';

import './style.css';

const Footer = () => (
	<div className="footer">
		<footer>© Copyright RPDM Corporation 2020. All rights reserved.</footer>
	</div>
);


export default Footer;
