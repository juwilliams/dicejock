import React, { useEffect, useMemo, useState } from 'react';

import {
	Button,
	Dialog,
	Label,
	PageRow,
	PageColumn,
	Select,
	TextField
} from 'ui-kit';

import {
	featAffectsTypes,
	featAffectsTypeOptions,
	featDurationTypeOptions
} from 'schema/feat';

const FeatDialog = props => {
	const { attributes, edit, feat, onAdd, onClose, open, skills } = props;
	const [name, setName] = useState(feat?.name || '');
	const [description, setDescription] = useState(feat?.description || '');
	const [duration, setDuration] = useState(feat?.duration || '');
	
	const [value, setValue] = useState('');
	const [requiredAttributeValue, setRequiredAttributeValue] = useState(0);

	//  select state
	const [selectedDurationTypeOption, setSelectedDurationTypeOption] = useState(undefined);
	const [selectedAffectsTypeOption, setSelectedAffectsTypeOption] = useState(undefined);
	const [selectedAffectsOption, setSelectedAffectsOption] = useState(undefined);
	const [selectedRequiredAttributeOption, setSelectedRequiredAttributeOption] = useState(
		undefined
	);

	const [affectsOptions, setAffectsOptions] = useState([]);
	const [attributeOptions, setAttributeOptions] = useState([]);
	const [skillOptions, setSkillOptions] = useState([]);

	useMemo(() => {
		if (attributes) {
			setAttributeOptions(
				attributes.map(attr => ({
					key: attr.name,
					label: attr.name,
					value: attr.name
				}))
			);
		}
	}, [attributes]);

	useMemo(() => {
		if (skills) {
			setSkillOptions(
				skills.map(skill => ({
					key: skill.name,
					label: skill.name,
					value: skill.name
				}))
			);
		}
	}, [skills]);

	useEffect(() => {
		if (selectedAffectsTypeOption) {
			switch (selectedAffectsTypeOption.value) {
				case featAffectsTypes.ATTRIBUTE:
					setAffectsOptions(attributeOptions);
					break;
				case featAffectsTypes.SKILL:
				case featAffectsTypes.SKILL_RANK:
				case featAffectsTypes.DC_CHECK:
					setAffectsOptions(skillOptions);
					break;
				default:
			}
		}
	}, [selectedAffectsTypeOption, attributeOptions, skillOptions]);

	useEffect(() => {
		setSelectedAffectsTypeOption(
			feat?.affectsType
				? featAffectsTypeOptions.find(o => o.value === feat.affectsType)
				: undefined || undefined
		);
		setSelectedAffectsOption(
			feat?.affects
				? affectsOptions.find(o => o.value === feat.affects)
				: undefined || undefined
		);
		setSelectedDurationTypeOption(
			feat?.durationType
				? featDurationTypeOptions.find(o => o.value === feat.durationType)
				: undefined || undefined
		);
		setSelectedRequiredAttributeOption(
			feat?.requiredAttribute && attributeOptions
				? attributeOptions.find(o => o.value === feat.requiredAttribute)
				: undefined || undefined
		);
		setRequiredAttributeValue(feat?.requiredAttributeValue || 0);
	}, [feat, affectsOptions, attributeOptions, skillOptions]);
	
	const handleAddClick = () => {
		if (onAdd) {
			const outFeat = {
				id: feat?.id,
				name,
				description,
				value,
				affectsType: selectedAffectsTypeOption
					? selectedAffectsTypeOption.value
					: undefined,
				affects: selectedAffectsOption ? selectedAffectsOption.value : undefined,
				durationType: selectedDurationTypeOption
					? selectedDurationTypeOption.value
					: undefined,
				duration,
				requiredAttribute: selectedRequiredAttributeOption
					? selectedRequiredAttributeOption.value
					: undefined,
				requiredAttributeValue
			};
			if (onAdd({ feat: outFeat, edit })) onClose();
		}
	};

	return (
		<Dialog
			open={open}
			onClose={onClose}
			actions={[
				<Button
					key="attribute-dialog-submit"
					value={edit ? 'Edit' : 'Add'}
					onClick={handleAddClick}
					primary
				/>
			]}>
			<Label value="Feat Configuration" />
			<PageRow nopadding>
				<PageColumn>
					<TextField
						label="Name"
						onChange={e => setName(e.target.value)}
						defaultValue={name}
					/>
					<Select
						label="Duration Type"
						options={featDurationTypeOptions}
						onChange={opt => setSelectedDurationTypeOption(opt)}
						defaultValue={selectedDurationTypeOption}
					/>
					<Select
						label="Required Attribute"
						options={attributeOptions}
						onChange={opt => setSelectedRequiredAttributeOption(opt)}
						defaultValue={selectedRequiredAttributeOption}
					/>
					<Select
						label="Affects Type"
						options={featAffectsTypeOptions}
						onChange={opt => setSelectedAffectsTypeOption(opt)}
						defaultValue={selectedAffectsTypeOption}
					/>
					<Select
						label="Affects"
						options={affectsOptions}
						onChange={opt => setSelectedAffectsOption(opt)}
						defaultValue={selectedAffectsOption}
					/>
				</PageColumn>
				<PageColumn>
					<TextField
						label="Value"
						onChange={e => setValue(e.target.value)}
						defaultValue={value}
					/>
					<TextField
						label="Duration"
						onChange={e => setDuration(e.target.value)}
						defaultValue={duration}
					/>
					<TextField
						label="Required Attribute Value"
						onChange={e => setRequiredAttributeValue(e.target.value)}
						defaultValue={requiredAttributeValue}
					/>
					<TextField
						label="Description"
						multiline
						rows={5}
						onChange={e => setDescription(e.target.value)}
						defaultValue={description}
					/>
				</PageColumn>
			</PageRow>
		</Dialog>
	);
};

export default FeatDialog;
