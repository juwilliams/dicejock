import React, { useEffect, useMemo, useState } from 'react';

import {
	Button,
	Dialog,
	Label,
	PageRow,
	PageColumn,
	Select,
	TextField
} from 'ui-kit';

import { AlignmentOptions } from 'schema/character';

const CharacterDialog = props => {
    const { open, edit, onAdd, onClose, character, characterTemplates } = props;
    
	const [name, setName] = useState(character?.name || '');
	const [level, setLevel] = useState(character?.level || '');
    const [biography, setBiography] = useState(character?.biography || '');
    const [faction, setFaction] = useState(character?.faction || '');
    const [attributes, setAttributes] = useState(undefined);
	
    //  options state
    const [characterTemplateOptions, setCharacterTemplateOptions] = useState([]);
    
	//  select state
	const [selectedAlignment, setSelectedAlignment] = useState('');
	const [selectedCharacterTemplate, setSelectedCharacterTemplate] = useState(undefined);
    
    useMemo(() => {
		setCharacterTemplateOptions(
			characterTemplates.map(template => ({
				key: template.name,
				label: template.name,
				value: template.name
			}))
		);
	}, [character, characterTemplates]);

	useEffect(() => {
		setName(character?.name || '');
		setBiography(character?.description || '');
		setFaction(character?.faction || '');
		setSelectedCharacterTemplate(characterTemplates?.find(template => template.name === character?.templateName));
		setSelectedAlignment(AlignmentOptions.find(a => a.value === character?.alignment)?.value);
	}, [character, characterTemplateOptions]);

    useEffect(() => {
		//	if this is the attribute currently assigned, we need to populate the attribute with the value
		setAttributes(selectedCharacterTemplate?.attributes?.map(attr => {
			const existingCharAttribute = character?.attributes?.find(charAttr => charAttr.id === attr.id);
			return {
				...attr,
				value: existingCharAttribute?.value
			};
		}));
            // setAttributes(selectedCharacterTemplate.attributes?.map(attr => ({ id: attr.id, name: attr.name, ...attr})));
    }, [selectedCharacterTemplate]);

    const handleAttributeChange = ({ id, name: attributeName, value }) => {
        setAttributes([
            ...attributes.filter(attr => attr.name !== attributeName),
            { id, name: attributeName, value }
        ]);
    }
    const handleAdd = () => {
        if (onAdd) {
            if (onAdd({
                name, alignment: selectedAlignment, biography, faction, attributes: attributes.map(attr => ({ id: attr.id, name: attr.name, value: attr.value })), level, templateName: selectedCharacterTemplate?.name, templateId: selectedCharacterTemplate?.id
            })) {
				onClose();
			}
        }
    }
    
	return (
		<Dialog
			open={open}
			onClose={onClose}
			actions={[
				<Button
					key="rule-dialog-actions-submit"
					value={edit ? 'Edit' : 'Add'}
					onClick={handleAdd}
					primary
				/>
			]}>
			<Label value="Character Configuration" />
			<PageRow nopadding>
				<PageColumn>
					<TextField
						label="Name"
						onChange={e => setName(e.target.value)}
						defaultValue={name}
					/>
					<TextField
						label="Level"
						onChange={e => setLevel(e.target.value)}
						defaultValue={level}
					/>
                    {selectedCharacterTemplate && selectedCharacterTemplate.attributes?.map(attr => (
                        <TextField key={`character-attr-${attr.id}`} defaultValue={character?.attributes.find(charAttr => charAttr.id === attr.id)?.value} label={attr.name} placeholder={`${attr.type} Min(${attr.minValue}) Max(${attr.maxValue})`} onChange={e => handleAttributeChange({ value: e.target.value, ...attr })} />
                    ))}
				</PageColumn>
				<PageColumn>
					<Select
						label="Template"
						options={characterTemplateOptions}
						onChange={opt => setSelectedCharacterTemplate(characterTemplates.find(temp => temp.name === opt.value))}
						defaultValue={characterTemplateOptions.find(opt => opt.value === selectedCharacterTemplate?.name)}
					/>
					<Select
						label="Alignment"
						options={AlignmentOptions}
						onChange={opt => setSelectedAlignment(opt.value)}
						defaultValue={AlignmentOptions.find(opt => opt.value === selectedAlignment)}
					/>
					<TextField
						label="Faction"
						onChange={e => setFaction(e.target.value)}
						defaultValue={faction}
					/>
					<TextField
						label="Biography"
						multiline
						rows={5}
						onChange={e => setBiography(e.target.value)}
						defaultValue={biography}
					/>
				</PageColumn>
			</PageRow>
		</Dialog>
	);
};

export default CharacterDialog;
