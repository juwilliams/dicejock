import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { authSelector } from 'state/auth/auth.selectors';

//	icon library and icons
import { library } from '@fortawesome/fontawesome-svg-core';
import {
	faBug,
	faCheckCircle,
	faCog,
	faExclamationCircle,
	faInfoCircle,
	faTimes
} from '@fortawesome/free-solid-svg-icons';

import { Footer } from 'components';

import { init } from 'state/auth/auth.reducer';
import store from 'store';
import { setLoggedOutInterceptor } from 'util/axiosClient';
import { getTemplatesAction } from 'state/templates/templates.actions';

const App = props => {
	const dispatch = useDispatch();
	
	const { initComplete, token } = useSelector(state => authSelector(state));

	const { children } = props;

	library.add([
		faBug,
		faCheckCircle,
		faCog,
		faExclamationCircle,
		faInfoCircle,
		faTimes
	]);

	setLoggedOutInterceptor(store);

	useEffect(()=> {
		if (!initComplete) dispatch(init());
		if (initComplete && token) dispatch(getTemplatesAction());
	}, [initComplete]);

	if (!initComplete) return <div className="loading">Loading..</div>;

	return (
		<div className="app">
			{children}
			<Footer />
		</div>
	);
};

export default App;
