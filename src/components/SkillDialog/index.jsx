import React, { useEffect, useMemo, useState } from 'react';

import {
	Button,
	Dialog,
	Label,
	PageRow,
	PageColumn,
	Select,
	TextField
} from 'ui-kit';

const SkillDialog = props => {
	const { attributes = [], open, edit, onAdd, onClose, skill } = props;
	const [name, setName] = useState(skill?.name || '');
	const [description, setDescription] = useState(skill?.description || '');
	const [difficulty, setDifficulty] = useState(skill?.difficulty || '');
	const [minDependentAttributeValue, setMinDependentAttributeValue] = useState(skill?.minDependentAttributeValue || '');
	const [ranks, setRanks] = useState('');

	//  select state
	const [attributeOptions, setAttributeOptions] = useState([]);
	const [selectedDependentAttributeOption, setSelectedDependentAttribute] = useState(
		undefined
	);

	useEffect(() => {
		setSelectedDependentAttribute(
			skill?.dependentAttribute
				? attributeOptions.find(attr => attr.value === skill.dependentAttribute)
				: undefined
		);
	}, [skill, attributeOptions]);

	useMemo(() => {
		if (attributes) {
			setAttributeOptions(
				attributes.map(attr => ({
					key: attr.name,
					label: attr.name,
					value: attr.name
				}))
			);
		}
	}, [attributes]);

	const handleAddClick = () => {
		if (onAdd) {
			const outSkill = {
				id: skill?.id,
				name,
				description,
				difficulty,
				minDependentAttributeValue,
				ranks,
				dependentAttribute: selectedDependentAttributeOption
					? selectedDependentAttributeOption.value
					: undefined
			};
			if (onAdd({ skill: outSkill, edit })) onClose();
		}
	};

	return (
		<Dialog
			open={open}
			onClose={onClose}
			actions={[
				<Button
					key="rule-dialog-actions-submit"
					value={edit ? 'Edit' : 'Add'}
					onClick={handleAddClick}
					primary
				/>
			]}>
			<Label value="Rule Configuration" />
			<PageRow nopadding>
				<PageColumn>
					<TextField
						label="Name"
						onChange={e => setName(e.target.value)}
						defaultValue={name}
					/>
					<TextField
						label="Difficulty"
						onChange={e => setDifficulty(e.target.value)}
						defaultValue={difficulty}
					/>
					<TextField
						label="Ranks"
						onChange={e => setRanks(e.target.value)}
						defaultValue={ranks}
					/>
				</PageColumn>
				<PageColumn>
					<Select
						label="Dependent Attribute"
						options={attributeOptions}
						onChange={opt => setSelectedDependentAttribute(opt)}
						defaultValue={selectedDependentAttributeOption}
					/>
					<TextField
						label="Minimum Dependent Attribute Value"
						onChange={e => setMinDependentAttributeValue(e.target.value)}
						defaultValue={minDependentAttributeValue}
					/>
					<TextField
						label="Description"
						multiline
						rows={5}
						onChange={e => setDescription(e.target.value)}
						defaultValue={description}
					/>
				</PageColumn>
			</PageRow>
		</Dialog>
	);
};

export default SkillDialog;
