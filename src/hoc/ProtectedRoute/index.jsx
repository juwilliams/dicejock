import React, { useEffect } from 'react';
import { Redirect, Route, withRouter } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import { authSelector } from 'state/auth/auth.selectors';

import { setInitialPath } from 'state/nav/nav.reducer';
import { navSelector } from 'state/nav/nav.selectors';

const ProtectedRoute = props => {
	const dispatch = useDispatch();

	const {
		component: RenderComponent,
		location: { pathname, search },
		path,
	} = props;

	const { token } = useSelector(state => authSelector(state));
	const { initialPath } = useSelector(state => navSelector(state));

	useEffect(() => {
		if (pathname === '/logout' && initialPath === '/logout') {
			dispatch(setInitialPath(undefined));
			return;
		}

		//	sets initial path and supports query params
		if (!token && !initialPath) setInitialPath(`${pathname}${search}`);
	}, [token, initialPath]);
	
	return (
		<Route
			path={path}
			render={renderProps => {
				if (token && initialPath) {
					dispatch(setInitialPath(undefined));
					return <Redirect to={initialPath} />;
				}
				if (token) {
					return <RenderComponent {...renderProps} />;
				}
				return <Redirect to="/login" />;
			}}
		/>
	);
}

export default withRouter(ProtectedRoute);