import React from 'react';
import { useSelector } from 'react-redux';

import { authSelector } from 'state/auth/auth.selectors';

import { Page } from 'ui-kit';
import { SiteNav } from 'components';

const PageWithSiteNav = (Inner, pageArgs) => props => {
	const { token } = useSelector(state => authSelector(state));

	return (
		<Page pageArgs={pageArgs} siteNavigation={<SiteNav isLoggedIn={token} />}>
			<Inner {...props} />
		</Page>
	);
};

export default PageWithSiteNav;
