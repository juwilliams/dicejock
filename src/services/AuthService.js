import axiosClient from 'util/axiosClient';

/**
 * Anything related to Authentication in the system.
 */
export default class AuthService {
  static login(email, password, captchaToken) {
    return axiosClient
      .post(`${process.env.REACT_APP_AUTH_SERVICES_URL}/token/login`, {
        email,
        password,
        captchaToken
      })
      .then((response) => response.data);
  }

  static register(username, email, password, captchaToken) {
    return axiosClient
      .post(`${process.env.REACT_APP_AUTH_SERVICES_URL}/account/register`, {
        username,
        email,
        password,
        captchaToken
      })
      .then((response) => response.data);
  }
}
