import axiosClient from 'util/axiosClient';

/**
 * Anything related to Authentication in the system.
 */
export default class PlayerService {
  static save(player) {
    return axiosClient
      .post(`${process.env.REACT_APP_DICEBOARD_SERVICES_URL}/players`, {
        player
      })
      .then((response) => response.data);
  }

  static getAll() {
    return axiosClient.get(`${process.env.REACT_APP_DICEBOARD_SERVICES_URL}/players`).then((response) => response.data);
  }
}
