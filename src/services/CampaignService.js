import axiosClient from 'util/axiosClient';

/**
 * Anything related to Authentication in the system.
 */
export default class CampaignService {
  static addPlayer({ invite, player }) {
    return axiosClient
      .post(`${process.env.REACT_APP_DICEBOARD_SERVICES_URL}/campaigns/add-player`, {
        invite,
        player
      })
      .then((response) => response.data);
  }

  static getAll() {
    return axiosClient
      .get(`${process.env.REACT_APP_DICEBOARD_SERVICES_URL}/campaigns`)
      .then((response) => response.data);
  }

  static get(id) {
    return axiosClient
      .get(`${process.env.REACT_APP_DICEBOARD_SERVICES_URL}/campaigns/${id}`)
      .then((response) => response.data);
  }

  static save(campaign) {
    console.log('campaign', campaign);
    return axiosClient
      .post(`${process.env.REACT_APP_DICEBOARD_SERVICES_URL}/campaigns`, {
        campaign
      })
      .then((response) => response.data);
  }
}
