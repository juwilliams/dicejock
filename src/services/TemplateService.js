import axiosClient from 'util/axiosClient';

/**
 * Anything related to Authentication in the system.
 */
export default class TemplateService {
  static save(template) {
    return axiosClient
      .post(`${process.env.REACT_APP_DICEBOARD_SERVICES_URL}/templates`, {
        template
      })
      .then((response) => response.data);
  }

  static getAll() {
    return axiosClient
      .get(`${process.env.REACT_APP_DICEBOARD_SERVICES_URL}/templates`)
      .then((response) => response.data);
  }

  static get(id) {
    return axiosClient
      .get(`${process.env.REACT_APP_DICEBOARD_SERVICES_URL}/templates/${id}`)
      .then((response) => response.data);
  }
}
