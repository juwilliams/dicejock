import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { addPlayerAction } from 'state/campaigns/campaigns.actions';
import { getPlayersAction } from 'state/players/players.actions';
import { setCurrentPlayer } from 'state/players/players.reducer';
import { playersSelector } from 'state/players/players.selectors';

import { PageWithSiteNav } from 'hoc';

import { playerGridHeaders } from 'schema/player';

import { Button, Grid, LinkButton, PageRow } from 'ui-kit';

const Join = props => {
	const {
		match: {
			params: { invite }
		},
	} = props;

	const dispatch = useDispatch();

	const { all: players, current: currentPlayer } = useSelector(state => playersSelector(state));

	const [selectedPlayer, setSelectedPlayer] = useState(undefined);

	useEffect(() => {
		if (getPlayersAction) dispatch(getPlayersAction());
	}, [getPlayersAction]);

	if (currentPlayer) return <Redirect push to="/player" />;

	const handleJoinClick = () => {
		dispatch(addPlayerAction({
			player: selectedPlayer,
			invite
		}));
	};

	return (
		<div className="campaign-join">
			<PageRow>
				<Grid
					actions={[
						<Button
							disabled={!selectedPlayer}
							onClick={handleJoinClick}
							value="Join"
							primary
							small
						/>,
						<LinkButton to="/player" primary small>
							Create Player
						</LinkButton>
					]}
					onSelection={player => setSelectedPlayer(player)}
					fullwidth
					title="Players"
					headers={playerGridHeaders}
					data={players}
					onIdClick={player => setCurrentPlayer({ player })}
				/>
			</PageRow>
		</div>
	);
};

export default PageWithSiteNav(Join, {
	title: 'A journey awaits you!',
	subTitle: 'select or create a player to begin',
	condenseHeader: true
});
