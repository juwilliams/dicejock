export const innerPageIds = {
	DETAILS: 'DETAILS',
	PLAYERS: 'PLAYERS',
	CHARACTERS: 'CHARACTERS',
	EVENTS: 'EVENTS',
	ITEMS: 'ITEMS',
	SCHEDULE: 'SCHEDULE'
};

export default {
	innerPageIds
};
