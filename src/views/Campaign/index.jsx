import React, { useState, useEffect, useMemo } from 'react';
import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { showDialog, hideDialog } from 'state/dialog/dialog.reducer';
import { getCampaignAction } from 'state/campaigns/campaigns.actions';
import { saveCampaign, setCurrentCampaign } from 'state/campaigns/campaigns.reducer'; 
import { campaignsSelector } from 'state/campaigns/campaigns.selectors';

import { PageWithSiteNav } from 'hoc';

import {
	Button,
	Grid,
	Label,
	PageColumn,
	PageInnard,
	PageInnerNav,
	PageRow,
	Select,
	TextField
} from 'ui-kit';

import { characterGridHeaders } from 'schema/character';
import { eventGridHeaders } from 'schema/event';
import { itemGridHeaders } from 'schema/item';
import { playerGridHeaders } from 'schema/player';

import { gameTemplatesSelector, characterTemplatesSelector, itemTemplatesSelector } from 'state/templates/templates.selectors';

import { CharacterDialog, EventDialog, ItemDialog } from 'components';
import { innerPageIds } from './model';

import Join from './Join';

const Campaign = () => {
	const dispatch = useDispatch();
	const gameTemplates = useSelector(state => gameTemplatesSelector(state));
	const characterTemplates = useSelector(state => characterTemplatesSelector(state));
	const itemTemplates = useSelector(state => itemTemplatesSelector(state));
	
	const { id } = useParams();

	const { current: campaign, isBusy } = useSelector((state) => campaignsSelector(state));

	const [activeInnerPageId, setActiveInnerPageId] = useState(
		innerPageIds.DETAILS
	);
	
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [players, setPlayers] = useState([]);
	const [playerInviteUrl, setPlayerInviteUrl] = useState('');
	const [characters, setCharacters] = useState([]);
	const [events, setEvents] = useState([]);
	const [items, setItems] = useState([]);

	//	grid state
	const [selectedRosterPlayers, setSelectedRosterPlayers] = useState([]);
	const [selectedRestingPlayers, setSelectedRestingPlayers] = useState(
		[]
	);
	const [selectedItems, setSelectedItems] = useState([]);
	const [selectedEvents, setSelectedEvents] = useState([]);
	
	//	select state
	const [selectedGameTemplate, setSelectedGameTemplate] = useState({});

	//	select options
	const [gameTemplateOptions, setGameTemplateOptions] = useState([]);

	//	load app state campaign into view state if exists on first load, then clear it out
	useMemo(() => {
		setGameTemplateOptions(
			gameTemplates.map(template => ({
				key: template.name,
				label: template.name,
				value: template.name
			}))
		);
	}, [campaign, gameTemplates]);

	useEffect(() => {
		setName(campaign?.name || '');
		setDescription(campaign?.description || '');
		setPlayers(campaign?.players || []);
		setPlayerInviteUrl(
			campaign
				? `${process.env.REACT_APP_SITE_URL}/campaign/join/${campaign.invite}`
				: ''
		);
		setCharacters(campaign?.characters || []);
		setSelectedGameTemplate(
			campaign && campaign.gameTemplate
				? gameTemplateOptions.find(
						template => template.value === campaign.gameTemplate
				  )
				: gameTemplateOptions[1] || undefined
		);
		setEvents(campaign?.events || []);
		setItems(campaign?.items || []);
	}, [campaign]);
	
	useEffect(() => {
		if (id) {
			dispatch(getCampaignAction(id));
		}
		return () => {
			dispatch(setCurrentCampaign({ campaign: undefined }));
		};
	}, [id]);

	const handleInnerPageNavClick = pageId => {
		setActiveInnerPageId(pageId);
	};
	const handleRestPlayersClick = () => {
		const rosterPlayers = selectedRosterPlayers.map(p => ({
			...p,
			active: false
		}));
		const allPlayers = [
			...players.filter(p => !rosterPlayers.find(r => r.id === p.id)),
			...rosterPlayers
		];
		setPlayers(allPlayers);
		setSelectedRosterPlayers([]);
	};
	const handleActivatePlayersClick = () => {
		const restingPlayers = selectedRestingPlayers.map(p => ({
			...p,
			active: true
		}));
		const allPlayers = [
			...players.filter(p => !restingPlayers.find(r => r.id === p.id)),
			...restingPlayers
		];
		setPlayers(allPlayers);
		setSelectedRestingPlayers([]);
	};
	const handleAddCharacter = character => {
		setCharacters([
			...characters.filter(c => c.name !== character.name),
			character	
		]);
		return true;
	};
	const handleShowCharacterDialog = character => {
		dispatch(
			showDialog({
				render: props => (
					<CharacterDialog {...props} />
				),
				props: {
					onAdd: handleAddCharacter,
					onClose: () => dispatch(hideDialog()),
					characterTemplates,
					character,
					edit: character
				}
			})
		);
	}
	const handleAddEvent = event => {
		setEvents([
			...events?.filter(c => c.name !== event.name) || [],
			event	
		]);
		return true;
	}
	const handleRemoveEvents = () => {
		if (!selectedEvents) return;

		setEvents([
			...events?.filter(e => !selectedEvents.find(se => se.name === e.name)),
		]);
	}
	const handleShowEventDialog = event => {
		dispatch(
			showDialog({
				render: props => (
					<EventDialog {...props} />
				),
				props: {
					onAdd: handleAddEvent,
					onClose: () => dispatch(hideDialog()),
					characterTemplates,
					characters,
					event,
					items,
					edit: event
				}
			})
		);
	}
	const handleAddItem = event => {
		setItems([
			...items?.filter(c => c.name !== event.name) || [],
			event	
		]);
		return true;
	}
	const handleRemoveItems = () => {
		if (!selectedItems) return;

		setEvents([
			...items?.filter(e => !selectedItems.find(se => se.name === e.name)),
		]);
	}
	const handleShowItemDialog = item => {
		console.log('item', item);
		dispatch(
			showDialog({
				render: props => (
					<ItemDialog {...props} />
				),
				props: {
					onAdd: handleAddItem,
					onClose: () => dispatch(hideDialog()),
					itemTemplates,
					item,
					edit: item
				}
			})
		);
	}
	const handleSaveClick = () => {
		dispatch(saveCampaign({
			id,
			characters,
			description,
			events,
			items,
			gameTemplate: selectedGameTemplate?.value || '',
			name,
			players
		}));
	};
	
	return (
		<div className="campaign-editor">
			<PageRow nopadding>
				<PageColumn>
					<PageInnerNav>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.DETAILS
							})}
							value="Details"
							onClick={() => handleInnerPageNavClick(innerPageIds.DETAILS)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.PLAYERS
							})}
							value="Players"
							onClick={() => handleInnerPageNavClick(innerPageIds.PLAYERS)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.CHARACTERS
							})}
							value="Characters"
							onClick={() => handleInnerPageNavClick(innerPageIds.CHARACTERS)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.EVENTS
							})}
							value="Events"
							onClick={() => handleInnerPageNavClick(innerPageIds.EVENTS)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.ITEMS
							})}
							value="Items"
							onClick={() => handleInnerPageNavClick(innerPageIds.ITEMS)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.SCHEDULE
							})}
							value="Schedule"
							onClick={() => handleInnerPageNavClick(innerPageIds.SCHEDULE)}
							secondary
							hoverable
							flat
						/>
						<Button
							value="Save Changes"
							onClick={handleSaveClick}
							primary
							flat
							async
							isBusy={isBusy}
						/>
					</PageInnerNav>
				</PageColumn>
				<PageColumn fullwidth>
					{activeInnerPageId && activeInnerPageId === innerPageIds.DETAILS && (
						<PageInnard id={innerPageIds.DETAILS}>
							<Label value="Details" />
							<TextField
								label="Name"
								disabled={!!id}
								defaultValue={name}
								onChange={e => setName(e.target.value)}
							/>
							<Select
								label="Game Template"
								options={gameTemplateOptions}
								onChange={opt => setSelectedGameTemplate(opt)}
								defaultValue={selectedGameTemplate}
							/>
							<TextField
								label="Description"
								defaultValue={description}
								onChange={e => setDescription(e.target.value)}
								multiline
								fullwidth
								rows={12}
							/>
							<TextField
								label="Player Invite URL"
								disabled
								defaultValue={playerInviteUrl}
							/>
						</PageInnard>
					)}
					{activeInnerPageId && activeInnerPageId === innerPageIds.PLAYERS && (
						<PageInnard id={innerPageIds.PLAYERS}>
							<Grid
								actions={
									<>
										<Button
											value="Rest Players"
											primary
											small
											onClick={() => handleRestPlayersClick()}
										/>
									</>
								}
								fullwidth
								onSelection={selection => setSelectedRosterPlayers(selection)}
								multipleSelection
								title="Active Players"
								headers={playerGridHeaders}
								data={players.filter(p => p.active)}
							/>
							<Grid
								actions={
									<>
										<Button
											value="Activate Players"
											primary
											small
											onClick={() => handleActivatePlayersClick()}
										/>
									</>
								}
								fullwidth
								onSelection={selection => setSelectedRestingPlayers(selection)}
								multipleSelection
								title="Resting Players"
								headers={playerGridHeaders}
								data={players.filter(p => !p.active)}
							/>
						</PageInnard>
					)}
					{activeInnerPageId &&
						activeInnerPageId === innerPageIds.CHARACTERS && (
							<PageInnard id={innerPageIds.CHARACTERS}>
								<Grid
									actions={
										<>
											<Button
												value="Add Character"
												primary
												small
												onClick={() => handleShowCharacterDialog()}
											/>
										</>
									}
									onIdClick={handleShowCharacterDialog}
									fullwidth
									onSelection={selection => setSelectedRosterPlayers(selection)}
									multipleSelection
									title="Characters"
									headers={characterGridHeaders}
									data={characters}
								/>
							</PageInnard>
						)}
					{activeInnerPageId && activeInnerPageId === innerPageIds.EVENTS && (
						<PageInnard id={innerPageIds.EVENTS}>
							<Grid
									actions={
										<>
											<Button
												value="Add Event"
												primary
												small
												onClick={() => handleShowEventDialog()}
											/>
											<Button
												value="Remove Events"
												primary
												small
												onClick={handleRemoveEvents}
											/>
										</>
									}
									onIdClick={handleShowEventDialog}
									fullwidth
									onSelection={selection => setSelectedEvents(selection)}
									multipleSelection
									title="Events"
									headers={eventGridHeaders}
									data={events}
								/>
						</PageInnard>
					)}
					{activeInnerPageId && activeInnerPageId === innerPageIds.ITEMS && (
						<PageInnard id={innerPageIds.ITEMS}>
							<Grid
								actions={
									<>
										<Button
											value="Add Item"
											primary
											small
											onClick={() => handleShowItemDialog()}
										/>
										<Button
											value="Remove Item"
											primary
											small
											onClick={handleRemoveItems}
										/>
									</>
								}
								onIdClick={handleShowItemDialog}
								fullwidth
								onSelection={selection => setSelectedItems(selection)}
								multipleSelection
								title="Items"
								headers={itemGridHeaders}
								data={items}
							/>
						</PageInnard>
					)}
					{activeInnerPageId && activeInnerPageId === innerPageIds.SCHEDULE && (
						<PageInnard id={innerPageIds.SCHEDULE}>Schedule</PageInnard>
					)}
				</PageColumn>
			</PageRow>
		</div>
	);
};

export default PageWithSiteNav(Campaign, {
	title: 'Campaign Editor',
	subTitle: 'worlds imagined never end',
	condenseHeader: true
});

export { Join };
