import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { authSelector } from 'state/auth/auth.selectors';
import { logoutAction } from 'state/auth/auth.actions';

const Logout = () => {
	const dispatch = useDispatch();
	const { token } = useSelector(state => authSelector(state));

	useEffect(() => {
		if (token) dispatch(logoutAction());
	}, [token]);

	if (!token) return <Redirect to="/" />
	return <div className="logout">Logging you out..</div>;
}

export default Logout;
