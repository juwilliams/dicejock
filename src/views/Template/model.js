export const innerPageIds = {
	DETAILS: 'DETAILS',
	RULES: 'RULES',
	ATTRIBUTES: 'ATTRIBUTES',
	SKILLS: 'SKILLS',
	FEATS: 'FEATS',
	ITEMS: 'ITEMS',
	NOTES: 'NOTES'
};

export default {
	innerPageIds
};
