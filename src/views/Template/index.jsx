import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import classNames from 'classnames';

import { useDispatch, useSelector } from 'react-redux';

import { showDialog, hideDialog } from 'state/dialog/dialog.reducer';

import { getTemplateAction, getTemplatesAction } from 'state/templates/templates.actions';
import { saveTemplate, setCurrentTemplate } from 'state/templates/templates.reducer';
import { templatesSelector } from 'state/templates/templates.selectors';

import { PageWithSiteNav } from 'hoc';

import {
	Button,
	Grid,
	Label,
	PageColumn,
	PageInnard,
	PageInnerNav,
	PageRow,
	Select,
	TextField
} from 'ui-kit';

import {
	AttributeDialog,
	FeatDialog,
	RuleDialog,
	SkillDialog
} from 'components';


import { ruleGridHeaders } from 'schema/rule';
import { attributeGridHeaders } from 'schema/attribute';
import { skillGridHeaders } from 'schema/skill';
import { featGridHeaders } from 'schema/feat';
import {
	templateGridHeaders,
	templateTypes,
	templateTypeOptions
} from 'schema/template';

import { innerPageIds } from './model';

const Template = () => {
	const dispatch = useDispatch();

	const {
		all: items,
		isBusy,
		current: template,
	} = useSelector(state => templatesSelector(state));

	const { id, type } = useParams();

	const history = useHistory();

	//  ui state
	const [activeInnerPageId, setActiveInnerPageId] = useState(
		innerPageIds.DETAILS
	);

	//  template state
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [notes, setNotes] = useState('');

	//	rule state
	const [rules, setRules] = useState([]);
	const [selectedRule, setSelectedRule] = useState(undefined);
	const [selectedRules, setSelectedRules] = useState([]);

	//	attribute state
	const [attributes, setAttributes] = useState([]);
	const [selectedAttribute, setSelectedAttribute] = useState(undefined);
	const [selectedAttributes, setSelectedAttributes] = useState([]);

	//	skill state
	const [skills, setSkills] = useState([]);
	const [selectedSkill, setSelectedSkill] = useState(undefined);
	const [selectedSkills, setSelectedSkills] = useState([]);

	const [feats, setFeats] = useState([]);
	const [selectedFeat, setSelectedFeat] = useState(undefined);
	const [selectedFeats, setSelectedFeats] = useState([]);

	const [selectedTemplateType, setSelectedTemplateType] = useState(undefined);

	//	load app state campaign into view state if exists on first load, then clear it out
	useEffect(() => {
		if (template) {
			setName(template?.name || '');
			setAttributes(template?.attributes || []);
			setDescription(template?.description || '');
			setRules(template?.rules || []);
			setSkills(template?.skills || []);
			setFeats(template?.feats || []);
			setSelectedTemplateType(
				template
					? templateTypeOptions.find(o => o.value === template.type)
					: undefined
			);
			setNotes(template?.notes || '');

			setActiveInnerPageId(innerPageIds.DETAILS);
		}
	}, [template]);

	useEffect(() => {
		if (id && type) {
			dispatch(getTemplatesAction());
			dispatch(getTemplateAction(id));
		}
		return () => {
			dispatch(setCurrentTemplate({ template: undefined }));
		};
	}, [getTemplateAction, getTemplatesAction, id, type]);

	//  ui event handlers
	const handleInnerPageNavClick = pageId => {
		setActiveInnerPageId(pageId);
	};


	//	rule ui event handlers
	const handleAddRuleClick = ({ rule, edit }) => {
		if (edit) {
			setRules([...rules.filter(r => r.name !== selectedRule.name), rule]);
		} else {
			setRules([...rules, rule]);
		}

		setSelectedRule(undefined);
		return true;
	};
	const handleShowRuleDialog = rule => {
		dispatch(
			showDialog({
				render: props => (
					<RuleDialog {...props} />
				),
				props: {
					onAdd: handleAddRuleClick,
					onClose: () => dispatch(hideDialog()),
					rule
				}
			})
		)
	}
	const handleRemoveSelectedRulesClick = () => {
		if (selectedRules) {
			setRules([
				...rules.filter(r => !selectedRules.find(s => s.name === r.name))
			]);
		}
	};

	//	attribute ui event handlers
	const handleAddAttributeClick = ({ attribute, edit }) => {
		if (edit) {
			setAttributes([
				...attributes.filter(r => r.name !== selectedAttribute.name),
				attribute
			]);
		} else {
			setAttributes([...attributes, attribute]);
		}

		setSelectedAttribute(undefined);
		return true;
	};
	const handleShowAttributeDialog = attribute => {
		dispatch(
			showDialog({
				render: props => (
					<AttributeDialog {...props} />
				),
				props: {
					onAdd: handleAddAttributeClick,
					onClose: () => dispatch(hideDialog()),
					attribute
				}
			})
		)
	}
	const handleRemoveSelectedAttributesClick = () => {
		if (selectedAttributes) {
			setAttributes([
				...attributes.filter(
					r => !selectedAttributes.find(s => s.name === r.name)
				)
			]);
		}
	};

	//	skill ui event handlers
	const handleAddSkillClick = ({ skill, edit }) => {
		if (edit) {
			setSkills([...skills.filter(r => r.name !== selectedSkill.name), skill]);
		} else {
			setSkills([...skills, skill]);
		}

		setSelectedSkill(undefined);
		return true;
	};
	const handleShowSkillDialog = skill => {
		dispatch(
			showDialog({
				render: props => (
					<SkillDialog {...props} />
				),
				props: {
					onAdd: handleAddSkillClick,
					onClose: () => {
						dispatch(hideDialog())
					},
					skill,
					attributes
				}
			})
		)
	}
	const handleRemoveSelectedSkillsClick = () => {
		if (selectedSkills) {
			setSkills([
				...skills.filter(r => !selectedSkills.find(s => s.name === r.name))
			]);
		}
	};

	//	feat ui event handlers
	const handleAddFeatClick = ({ feat, edit }) => {
		if (edit) {
			setFeats([...feats.filter(r => r.name !== selectedFeat.name), feat]);
		} else {
			setFeats([...feats, feat]);
		}

		setSelectedFeat(undefined);
		return true;
	};
	const handleShowFeatDialog = feat => {
		dispatch(
			showDialog({
				render: props => (
					<FeatDialog {...props} />
				),
				props: {
					onAdd: handleAddFeatClick,
					onClose: () => {
						dispatch(hideDialog())
					},
					feat,
					attributes
				}
			})
		)
	}
	const handleRemoveSelectedFeatsClick = () => {
		if (selectedFeats) {
			setFeats([
				...feats.filter(r => !selectedFeats.find(s => s.name === r.name))
			]);
		}
	};

	//	save template handler
	const handleSaveClick = () => {
		if (!name || !selectedTemplateType) return;

		dispatch(saveTemplate({
			id,
			name,
			notes,
			description,
			type: selectedTemplateType.value,
			attributes,
			rules,
			skills,
			feats
		}));
	};

	return (
		<div className="template-editor">
			<PageRow nopadding>
				<PageColumn>
					<PageInnerNav>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.DETAILS
							})}
							value="Details"
							onClick={() => handleInnerPageNavClick(innerPageIds.DETAILS)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.RULES
							})}
							value="Rules"
							onClick={() => handleInnerPageNavClick(innerPageIds.RULES)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.ATTRIBUTES
							})}
							value="Attributes"
							onClick={() => handleInnerPageNavClick(innerPageIds.ATTRIBUTES)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.SKILLS
							})}
							value="Skills"
							onClick={() => handleInnerPageNavClick(innerPageIds.SKILLS)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.FEATS
							})}
							value="Feats"
							onClick={() => handleInnerPageNavClick(innerPageIds.FEATS)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.ITEMS
							})}
							value="Items"
							onClick={() => handleInnerPageNavClick(innerPageIds.ITEMS)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.NOTES
							})}
							value="Notes"
							onClick={() => handleInnerPageNavClick(innerPageIds.NOTES)}
							secondary
							hoverable
							flat
						/>
						<Button
							value="Save Changes"
							onClick={handleSaveClick}
							primary
							flat
							async
							isBusy={isBusy}
						/>
					</PageInnerNav>
				</PageColumn>
				<PageColumn fullwidth>
					{activeInnerPageId && activeInnerPageId === innerPageIds.DETAILS && (
						<PageInnard id={innerPageIds.DETAILS}>
							<Label value="Details" />
							<TextField
								label="Name"
								disabled={!!template}
								defaultValue={name}
								onChange={e => setName(e.target.value)}
							/>
							<Select
								label="Template Type"
								options={templateTypeOptions}
								onChange={opt => setSelectedTemplateType(opt)}
								defaultValue={selectedTemplateType}
							/>
							<TextField
								label="Description"
								defaultValue={description}
								onChange={e => setDescription(e.target.value)}
								multiline
								fullwidth
								rows={12}
							/>
						</PageInnard>
					)}
					{activeInnerPageId && activeInnerPageId === innerPageIds.RULES && (
						<PageInnard id={innerPageIds.RULES}>
							<Grid
								actions={
									<>
										<Button
											value="Create Rule"
											primary
											small
											onClick={handleShowRuleDialog}
										/>
										<Button
											value="Remove Rule"
											primary
											small
											onClick={handleRemoveSelectedRulesClick}
										/>
									</>
								}
								onIdClick={handleShowRuleDialog}
								fullwidth
								onSelection={selection => setSelectedRules(selection)}
								multipleSelection
								title="Rules"
								headers={ruleGridHeaders}
								data={rules}
							/>
						</PageInnard>
					)}
					{activeInnerPageId && activeInnerPageId === innerPageIds.ATTRIBUTES && (
						<PageInnard id={innerPageIds.ATTRIBUTES}>
							<Grid
								actions={
									<>
										<Button
											value="Create Attribute"
											primary
											small
											onClick={handleShowAttributeDialog}
										/>
										<Button
											value="Remove Attribute"
											primary
											small
											onClick={handleRemoveSelectedAttributesClick}
										/>
									</>
								}
								onIdClick={handleShowAttributeDialog}
								fullwidth
								onSelection={selection => setSelectedAttributes(selection)}
								multipleSelection
								title="Attributes"
								headers={attributeGridHeaders}
								data={attributes}
							/>
						</PageInnard>
					)}
					{activeInnerPageId && activeInnerPageId === innerPageIds.SKILLS && (
						<PageInnard id={innerPageIds.SKILLS}>
							<Grid
								actions={
									<>
										<Button
											value="Create Skill"
											primary
											small
											onClick={handleShowSkillDialog}
										/>
										<Button
											value="Remove Skill"
											primary
											small
											onClick={handleRemoveSelectedSkillsClick}
										/>
									</>
								}
								onIdClick={handleShowSkillDialog}
								fullwidth
								onSelection={selection => setSelectedSkills(selection)}
								multipleSelection
								title="Skills"
								headers={skillGridHeaders}
								data={skills}
							/>
						</PageInnard>
					)}
					{activeInnerPageId && activeInnerPageId === innerPageIds.FEATS && (
						<PageInnard id={innerPageIds.FEATS}>
							<Grid
								actions={
									<>
										<Button
											value="Create Feat"
											primary
											small
											onClick={handleShowFeatDialog}
										/>
										<Button
											value="Remove Feat"
											primary
											small
											onClick={handleRemoveSelectedFeatsClick}
										/>
									</>
								}
								onIdClick={handleShowFeatDialog}
								fullwidth
								onSelection={selection => setSelectedFeats(selection)}
								multipleSelection
								title="Feats"
								headers={featGridHeaders}
								data={feats}
							/>
						</PageInnard>
					)}
					{activeInnerPageId && activeInnerPageId === innerPageIds.ITEMS && (
						<PageInnard id={innerPageIds.ITEMS}>
							<Grid
								fullwidth
								title="Items"
								headers={templateGridHeaders}
								data={items}
								onIdClick={item =>
									history.push(
										`/template/${templateTypes.ITEM.toLowerCase()}/${item.id}`
									)
								}
							/>
						</PageInnard>
					)}
					{activeInnerPageId && activeInnerPageId === innerPageIds.NOTES && (
						<PageInnard id={innerPageIds.NOTES}>
							<TextField
								label="Notes"
								multiline
								rows={20}
								fullwidth
								defaultValue={notes}
								onChange={e => setNotes(e.target.value)}
							/>
						</PageInnard>
					)}
				</PageColumn>
			</PageRow>
		</div>
	);
};

export default PageWithSiteNav(Template, {
	title: 'Template Editor',
	subTitle: "magic or metal, it's up to you",
	condenseHeader: true
})
