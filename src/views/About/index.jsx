import React from 'react';

import { PageRow } from 'ui-kit';

import { PageWithSiteNav } from 'hoc';

const About = () => (
	<div className="about">
		<PageRow />
	</div>
);

export default PageWithSiteNav(About, {
	title: 'Build epic adventures',
	subTitle: 'share them with the world',
	darkMode: true
});
