import React from 'react';
import { PageWithSiteNav } from 'hoc';

const Features = () => (
	<div className="features">Features..</div>
);

export default PageWithSiteNav(Features, {
	title: 'Imagine a world without limits',
	subTitle: 'never lose a character sheet again'
});
