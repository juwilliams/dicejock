import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { getCampaignsAction } from 'state/campaigns/campaigns.actions';
import { campaignsSelector } from 'state/campaigns/campaigns.selectors';

import { PageWithSiteNav } from 'hoc';

import { Grid, PageRow, LinkButton } from 'ui-kit';

import { campaignGridHeaders } from 'schema/campaign';

const Journal = () => {
	const dispatch = useDispatch();

	const { all: campaigns } = useSelector(state => campaignsSelector(state));
	
	const history = useHistory();

	useEffect(() => {
		if (getCampaignsAction) dispatch(getCampaignsAction());
	}, [getCampaignsAction]);

	return (
		<div className="journal">
			<PageRow>
				<Grid
					actions={
						<>
							<LinkButton to="/campaign" primary small>
								Create Campaign
							</LinkButton>
						</>
					}
					fullwidth
					title="Campaigns"
					headers={campaignGridHeaders}
					data={campaigns}
					onIdClick={campaign => history.push(`/campaign/${campaign.id}`)}
				/>
			</PageRow>
		</div>
	);
};

export default PageWithSiteNav(Journal, {
	title: 'Journal',
	subTitle: 'continue your journey below',
	condenseHeader: true
});
