import React, { useState } from 'react';
import Reaptcha from 'reaptcha';
import { Link, Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { PageWithSiteNav } from 'hoc';

import { Button, ErrorMessage, TextField } from 'ui-kit';

import { login } from 'state/auth/auth.reducer';
import { authSelector } from 'state/auth/auth.selectors';

import './style.scss';

const Login = () => {
	const dispatch = useDispatch();
	const { isAuthenticating, token, authError } = useSelector((state) => authSelector(state));

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [captchaVerified, setCaptchaVerified] = useState(false);
	const [captchaToken, setCaptchaToken] = useState('');
	const [captchaRef, setCaptchaRef] = useState(undefined);

	const handleLoginClick = () => {
		dispatch(login({ email, password, captchaToken }));
		captchaRef.reset();
		setCaptchaVerified(false);
		setCaptchaToken('');
	};

	const onVerify = verifyToken => {
		setCaptchaVerified(true);
		setCaptchaToken(verifyToken);
	};

	if (token) {
		return <Redirect to="/journal" />;
	}

	return (
		<div className="login">
			<h3 className="title">Hail and well met adventurer!</h3>
			<h4 className="sub-title">Log in to continue your journey</h4>
			<div className="login-controls">
				{authError && <ErrorMessage error={authError} />}
				<TextField label="Email" onChange={e => setEmail(e.target.value)} />
				<TextField
					onChange={e => setPassword(e.target.value)}
					password
					label="Password"
					labelControls={[
						<Link key="password-reset" to="/password-reset">
							Reset Password
						</Link>
					]}
				/>
				<Button
					value="Log in to your account"
					async
					isBusy={isAuthenticating}
					primary
					onClick={handleLoginClick}
					disabled={!captchaVerified}
					nomarginleft
				/>
				<Reaptcha
					sitekey={process.env.REACT_APP_RECAPTCHA_SITE_KEY}
					onVerify={onVerify}
					ref={e => setCaptchaRef(e)}
				/>
			</div>
		</div>
	);
};

export default PageWithSiteNav(Login);