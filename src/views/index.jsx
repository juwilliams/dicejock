import About from './About';
import Campaign, { Join } from './Campaign';
import Designer from './Designer';
import Features from './Features';
import Journal from './Journal';
import Login from './Login';
import Logout from './Logout';
import Player from './Player';
import Register from './Register';
import Template from './Template';

export {
	About,
	Campaign,
	Designer,
	Features,
	Join,
	Journal,
	Login,
	Logout,
	Player,
	Register,
	Template
};
