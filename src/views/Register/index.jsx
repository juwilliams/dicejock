import React, { useState } from 'react';
import Reaptcha from 'reaptcha';
import { Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { PageWithSiteNav } from 'hoc';

import { registerAction } from 'state/auth/auth.actions';

import { Button, ErrorMessage, TextField } from 'ui-kit';

import './style.scss';
import { authSelector } from 'state/auth/auth.selectors';

const Register = () => {
	const dispatch = useDispatch();
	const { authError, token } = useSelector(state => authSelector(state));

	const [username, setUsername] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [captchaVerified, setCaptchaVerified] = useState(false);
	const [captchaToken, setCaptchaToken] = useState('');
	const [captchaRef, setCaptchaRef] = useState(undefined);

	const handleCreateAccountClick = () => {
		dispatch(registerAction({ username, email, password, captchaToken }));
		captchaRef.reset();
		setCaptchaVerified(false);
		setCaptchaToken('');
	};

	const onVerify = verifyToken => {
		setCaptchaVerified(true);
		setCaptchaToken(verifyToken);
	};

	if (token) {
		return <Redirect to="/journal" />;
	}

	return (
		<div className="register">
			<h3 className="title">It&#39;s dangerous to go alone!</h3>
			<h4 className="sub-title">Create an account to get started</h4>
			<div className="login-controls">
				{authError && <ErrorMessage error={authError} />}
				<TextField
					label="Username"
					onChange={e => setUsername(e.target.value)}
				/>
				<TextField label="Email" onChange={e => setEmail(e.target.value)} />
				<TextField
					onChange={e => setPassword(e.target.value)}
					password
					label="Password"
				/>
				<Button
					value="Create Account"
					primary
					onClick={handleCreateAccountClick}
					disabled={!captchaVerified}
				/>
				<Reaptcha
					sitekey={process.env.REACT_APP_RECAPTCHA_SITE_KEY}
					onVerify={onVerify}
					ref={e => setCaptchaRef(e)}
				/>
			</div>
		</div>
	);
};

export default PageWithSiteNav(Register);

// const mapStateToProps = state => ({
// 	authError: state.auth.error,
// 	token: state.auth.token
// });

// const mapDispatchToProps = dispatch =>
// 	bindActionCreators(
// 		{
// 			register: actions.register
// 		},
// 		dispatch
// 	);

// export default connect(
// 	mapStateToProps,
// 	mapDispatchToProps
// )(PageWithSiteNav(Register));
