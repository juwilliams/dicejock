import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { getTemplatesAction } from 'state/templates/templates.actions';
import { templatesSelector } from 'state/templates/templates.selectors';

import { PageWithSiteNav } from 'hoc';

import { Grid, PageRow, LinkButton } from 'ui-kit';

import { templateGridHeaders } from 'schema/template';

const Designer = () => {
	const dispatch = useDispatch();

	const { all: templates } = useSelector(state => templatesSelector(state));

	const history = useHistory();

	useEffect(() => {
		if (getTemplatesAction) dispatch(getTemplatesAction());
	}, [getTemplatesAction]);

	return (
		<div className="designer">
			<PageRow>
				<Grid
					actions={
						<>
							<LinkButton to="/template" primary small>
								Create Template
							</LinkButton>
						</>
					}
					fullwidth
					title="Templates"
					headers={templateGridHeaders}
					data={templates}
					onIdClick={template =>
						history.push(
							`/template/${template.type.toLowerCase()}/${template.id}`
						)
					}
				/>
			</PageRow>
		</div>
	);
};

export default PageWithSiteNav(Designer, {
	title: 'Designer',
	subTitle: "want to change the world, there's nothing to it",
	condenseHeader: true
});
