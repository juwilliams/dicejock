import React, { useState, useEffect, useCallback } from 'react';
import classNames from 'classnames';

import { useDispatch, useSelector } from 'react-redux';

import { savePlayer, setCurrentPlayer } from 'state/players/players.reducer';
import { playersSelector } from 'state/players/players.selectors';

import { PageWithSiteNav } from 'hoc';

import {
	Button,
	Label,
	PageColumn,
	PageInnard,
	PageInnerNav,
	PageRow,
	TextField
} from 'ui-kit';

import { innerPageIds } from './model';

const Player = () => {
	const dispatch = useDispatch();
	const { isBusy, current: player, } = useSelector(state => playersSelector(state));

	const [activeInnerPageId, setActiveInnerPageId] = useState(
		innerPageIds.DETAILS
	);
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [viewStatePlayer, setViewStatePlayer] = useState(undefined);

	const editPlayer = useCallback(() => {
		setName(viewStatePlayer.name);
		setDescription(viewStatePlayer.description);
	}, [viewStatePlayer]);

	//	load app state campaign into view state if exists on first load, then clear it out
	useEffect(() => {
		if (player) {
			setViewStatePlayer(player);
			dispatch(setCurrentPlayer({ player: undefined }));
		}
	}, [player, setViewStatePlayer, setCurrentPlayer]);

	useEffect(() => {
		if (viewStatePlayer) editPlayer();
	}, [viewStatePlayer, editPlayer]);

	const handleInnerPageNavClick = pageId => {
		setActiveInnerPageId(pageId);
	};
	const handleSaveClick = () => {
		dispatch(savePlayer({
			name,
			description
		}));
	};
	return (
		<div className="campaign-editor">
			<PageRow nopadding>
				<PageColumn>
					<PageInnerNav>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.DETAILS
							})}
							value="Details"
							onClick={() => handleInnerPageNavClick(innerPageIds.DETAILS)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.ATTRIBUTES
							})}
							value="Attributes"
							onClick={() => handleInnerPageNavClick(innerPageIds.ATTRIBUTES)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.SKILLS
							})}
							value="Skills"
							onClick={() => handleInnerPageNavClick(innerPageIds.SKILLS)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.FEATS
							})}
							value="Feats"
							onClick={() => handleInnerPageNavClick(innerPageIds.FEATS)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.ITEMS
							})}
							value="Items"
							onClick={() => handleInnerPageNavClick(innerPageIds.ITEMS)}
							secondary
							hoverable
							flat
						/>
						<Button
							className={classNames({
								active: activeInnerPageId === innerPageIds.NOTES
							})}
							value="Notes"
							onClick={() => handleInnerPageNavClick(innerPageIds.NOTES)}
							secondary
							hoverable
							flat
						/>
						<Button
							value="Save Changes"
							onClick={handleSaveClick}
							primary
							flat
							async
							isBusy={isBusy}
						/>
					</PageInnerNav>
				</PageColumn>
				<PageColumn fullwidth>
					{activeInnerPageId && activeInnerPageId === innerPageIds.DETAILS && (
						<PageInnard id={innerPageIds.DETAILS}>
							<Label value="Details" />
							<TextField
								label="Name"
								disabled={!!viewStatePlayer}
								value={name}
								onChange={e => setName(e.target.value)}
							/>
							<TextField
								label="Description"
								value={description}
								onChange={e => setDescription(e.target.value)}
								multiline
								fullwidth
								rows={12}
							/>
						</PageInnard>
					)}
					{activeInnerPageId &&
						activeInnerPageId === innerPageIds.ATTRIBUTES && (
							<PageInnard id={innerPageIds.ATTRIBUTES}>Attributes</PageInnard>
						)}
					{activeInnerPageId && activeInnerPageId === innerPageIds.SKILLS && (
						<PageInnard id={innerPageIds.SKILLS}>Skills</PageInnard>
					)}
					{activeInnerPageId && activeInnerPageId === innerPageIds.FEATS && (
						<PageInnard id={innerPageIds.FEATS}>Feats</PageInnard>
					)}
					{activeInnerPageId && activeInnerPageId === innerPageIds.ITEMS && (
						<PageInnard id={innerPageIds.ITEMS}>Items</PageInnard>
					)}
					{activeInnerPageId && activeInnerPageId === innerPageIds.NOTES && (
						<PageInnard id={innerPageIds.NOTES}>Notes</PageInnard>
					)}
				</PageColumn>
			</PageRow>
		</div>
	);
};

export default PageWithSiteNav(Player, {
	title: 'Player Editor',
	subTitle: "magic or metal, it's up to you",
	condenseHeader: true
});
