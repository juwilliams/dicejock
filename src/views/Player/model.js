export const innerPageIds = {
	DETAILS: 'DETAILS',
	ATTRIBUTES: 'ATTRIBUTES',
	SKILLS: 'SKILLS',
	FEATS: 'FEATS',
	ITEMS: 'ITEMS',
	NOTES: 'NOTES'
};

export default {
	innerPageIds
};
