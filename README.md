# React UI for Diceboard

React based UI for diceboard services. Offers robust set of tools for tabletop roleplaying games.

## Depends on 
* [rpdm-auth](https://gitlab.com/juwilliams/rpdm-auth)
* [diceboard-services](https://gitlab.com/juwilliams/diceboard-services)

## Deployment
- NYI, though you can run the firebase emulators locally

## Environment variables (provided via public/js/globals.js - built by env-globals.sh locally)
1. RECAPTCHA_SITE_KEY, used to initiate recaptcha communication
2. DICEBOARD_SERVICES_URL, url for data services used by diceboards
3. AUTH_SERVICES_URL, url for authentication services used by all related products (login, token verification etc)

## How to use
1. Start rpdm-auth (nodemon server.js)
2. Start diceboard-services (nodemon server.js)
3. Start diceboard ui (npm run start)
