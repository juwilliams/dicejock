# dicejock

## Routing

## State Management
    - redux setup
    - redux-saga setup

## Authentication
    - redux/auth []
    - axiosClient []
    - password reset []
    - username lookup []
    - account registration []

## Content
    - Notification System []
    - Login []
    - Logout [x]
    - About []
    - Profile []
        - Games []
        - Characters []
    - Getting Started []
    - Journal []
        - PlaySessions []
        - Characters []
        - Messages []
        - Appendix (for current game) []
        - Lorebook []
    - Designer []
        - Appendix []
        - Campaign []
        - Character []
        - Place []
        - Item []
        - Spell []
        - Triggers []
            - Causes
                - Attacks
                - Enters
                - Exits
                - Interacts
                - Reputation
            - Effect
                - Attacks
                - Interacts
                - Reputation
    - GameBoard []
        - PlaySession (websocket streaming events) []
        - Current Place (Place) []
        - Characters []
        - Items []
        - Triggers []
        - Tools []
            - Dice []
            - Compendium []
    - Community (Browse public stuff, basically global version of journal)



## Building Blocks
    - Game
        - Player[]
        - Campaign
        - Name
        - PlaySession[]
        - LastPlaySession
        - NextPlaySession
        - OrganizedBy
    - Campaign
        - Version
        - Name
        - Chapter[]
            - Area[]
            - Creature[]
            - Event[]
    - Sheet
        - Version
        - Name (Duder Duderino)
        - FieldCollection[]
    - FieldCollection
        - Version
        - Name
        - Field[]
    - Field
        - Data Type
        - Field Type (Armor Class)
        - Value (0)
        - Name 'AC'
    - Field Types
        - Version
        - Name (Armor Class)
        - Effect (Damage Mitigation)
        - ValueRange { Start: -99, 99 }
    - Effect
        - Version (1)
        - Name (Damage Mitigation)
        - Impact (Add/Remove/[Block])
        - Immune (False)